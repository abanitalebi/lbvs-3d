function conspicuty_map = centre_surround(feature_map, labels, fovea_radius, segsize, fovea_mask)
    
    feature_map_padded = padarray(feature_map, [floor(fovea_radius/2),floor(fovea_radius/2)], 'symmetric');
    [height, width] = size(labels);
    conspicuty_map = zeros(height, width);
    diff_map = absdiff_calculator(feature_map, feature_map_padded, fovea_radius, fovea_mask);
    
    for k = 1:segsize
%         saliency_labels(k) = 0;
        segment_mask = double(labels==k);
        conspicuty_map = conspicuty_map + ((sum(sum(segment_mask .* diff_map)) / (sum(sum(segment_mask)))) * segment_mask);
    end
    
end

