function prob_map = prob_using_color_fixations(frame_rgb)
   
   [height, width, pp] = size(frame_rgb);
    
   % Color order: red, yellow, green, pink, orange, blue, cyan, magenta, light blue, maroon, violet, dark green
   p_fixations = [128, 87, 84, 60, 44, 32, 32, 26, 16, 14, 11, 10];
   p_fixations = p_fixations / sum(p_fixations);
   rgbs = [255,0,0; 255,255,0; 0,255,0; 255,192,203; 255,140,0; 0,0,255; 0,255,255; 255,0,255; 173,216,230; 128,0,0; 238,130,238; 0,100,0];
   x = double(reshape(frame_rgb, [height*width, 3]));
   IDX = knnsearch(rgbs, x);
   for i=1:length(IDX)
       p1d(i) = p_fixations(IDX(i));
   end
   p = reshape(p1d', [height, width]);
   prob_map = p / max(max(p));        
   
end