function discomfort_map = discomfort_penalizing_map(disparity_map, labels)

    segsize = max(labels(:));
    [height, width] = size(labels);
    
    map = zeros(height, width);
    for k = 1:segsize
        mask = double(labels==k);
        segment = mask .* disparity_map;
        d = abs(mean(segment(:)));
        if d <= 99
            y = 1;
        else
            y = 1.3355 - 0.0092*d;
        end
        map = map + y*mask;
    end
    if (max(map(:))-min(map(:))) ~= 0
        discomfort_map = (map-min(map(:))) / (max(map(:))-min(map(:)));
    else
        discomfort_map = map .* (map >= 0);
    end

end