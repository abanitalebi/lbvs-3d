function mask = fovea_mask(fovea_radius)

    % This creates a circular fovea mask
   
    mask = ones(fovea_radius);
    
    % First: Define the original fovea mask based on the density of the photoreceptors
    x0 = [197, 161, 125, 89, 74, 65, 58];        % Photoreceptors from 0 to 1 degree visual angle
    
    % Second: Change the 1D map to 2D
    n = length(x0);
    x1 = interp1(1:n, x0, 1:2*n, 'linear', 'extrap');               % extrapolate more elements
    x2 = zeros(2*n-1);  x2(n,1:n)=fliplr(x0);   x2(n,n:end)=x0;   x2(1:n,n)=fliplr(x0);   x2(n:end,n)=x0;
    for i = 1:2*n-1
        for j = 1:2*n-1
             d = sqrt((i-n)^2 + (j-n)^2);
             if (round(d)>d)
                 res = round(d)-d;
                 x2(i,j) = x1(round(d)+1) * (1-res) + x1(round(d))*res;
             end
             if (round(d)<=d)
                 res = d - round(d);
                 x2(i,j) = x1(round(d)+1) * (1-res) + x1(round(d)+2)*res;
             end
        end
    end
    
    % Third: Bring it to [0-1] and resize it to Fovea_radius             
    y = (x2-min(min(x2))) / (max(max(x2))-min(min(x2)));  % brings it to [0-1]
    mask = imresize(y, [2*fovea_radius+1, 2*fovea_radius+1]);
    
    NHOOD = fspecial('disk',fovea_radius)>0;
    mask = mask .* NHOOD;
    
end