% Ground-Truth and Saliency_Map are images in GrayScale format

function [emd, s_measure, areaUnderROC, FPRs, TPRs, FPs, TPs, RHO, KL, NSS, AUC_shuffled] = saliency_metrics_per_frame(ground_truth, saliency_map, shufMap)
%     
%     addpath('FastEMD-3.1');
%     addpath('shuffled-AUC');
        
    [height, width] = size(saliency_map);
    
    fixmap = double(ground_truth)/double(max(ground_truth(:)));
    map = double(saliency_map)/double(max(saliency_map(:)));
    
    % 1) Earth Mover's Distance: http://www.ariel.ac.il/sites/ofirpele/FastEMD/code/
    emd = calculateEMD(fixmap, map);    % it does the histogram matching itself
    
    % Perform histogram matching
    map_matched = imhistmatch(saliency_map, ground_truth);      
    map_matched = double(map_matched)/double(max(map_matched(:)));
    
    % 2) Similarity introducced by Judd
    s_measure = similarity(fixmap, map_matched);
    
    % 3) ROC analysis. ROC curve is TPR(y) vs FPR(x). http://en.wikipedia.org/wiki/Receiver_operating_characteristic
    % False Alarm = False Positive = FP
    % FPR = False Positive Rate = Fall_out = 1-Specificity
    % TPR = True Positive Rate = Recall = Sensitivity
    [areaUnderROC, FPRs, TPRs, FPs, TPs] = predictFixations(map_matched, fixmap);         
  
    % 4) Pearson Correlation ratio
    [RHO,PVAL] = corr(map_matched(:), fixmap(:));
    
    % Calculate the histograms
    [PC, temp1] = imhist(fixmap);
    [QC, temp2] = imhist(map_matched);
    P = PC / (height*width);
    Q = QC / (height*width);
    
    % 5) Kullback�Leibler divergence: http://en.wikipedia.org/wiki/Kullback%E2%80%93Leibler_divergence
    dists = KLDiv(fixmap, map_matched);
    KL = mean(dists(:));
    
    % 6) NSS
    bwfixmap = round(fixmap);
    score = calcNSSscore( map_matched, bwfixmap );
    NSS = mean(score);
    
    % 7) Shuffled AUC
    AUC_shuffled = calcAUCscore(map_matched, bwfixmap, shufMap);
    
end