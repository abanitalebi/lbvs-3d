function write_yuv_folder_frames(saliency_folder_path, subjects_frames, nof, numFiles)
    
    for f = 1:numFiles
        File = strcat (saliency_folder_path, '\file', num2str(f) , '.yuv');
        
        for i = 1:nof
            frame0 = imresize(subjects_frames(:,:,i,f), 2);
            frame = uint8(255*frame0);
            mov_f(i).cdata(:,:,1) = frame;
            mov_f(i).cdata(:,:,2) = frame;
            mov_f(i).cdata(:,:,3) = frame;
            mov_f(i).colormap = [];
        end
        mov2yuv(File,mov_f,'420')
    end
    
end
