%
% Author: Marius Drulea
% http://www.cv.utcluj.ro/optical-flow.html

% Reference
% Drulea, M.; Nedevschi, S., "Motion Estimation Using the Correlation Transform," 
% Image Processing, IEEE Transactions on , vol.22, no.8, pp.3260,3270, Aug. 2013

% Copyright (C) 2013 Technical University of Cluj-Napoca

% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

% CPP FILES:
% the software has been tested on W7x64. For other OS-s it is necessary
% to recompile the cpp files calling the function "compile_all_cpp_files"
%%
close all;
clear;

% load settings
st = load_settings('correlation_basic');
% st = load_settings('correlation_full');
% st = load_settings('census_basic');
% st = load_settings('intensity_basic');

show_flow = 1; % 1 = display the evolution of the flow, 0 = do not show
h = figure('Name', 'Optical flow');

I1 = double(imread('data/Amin/f1.bmp'))/255;
I2 = double(imread('data/Amin/f2.bmp'))/255;

% I1 = double(imread('data/Grove3/Grove3_illumination_10.png'))/255;
% I2 = double(imread('data/Grove3/Grove3_illumination_11.png'))/255;
% I1 = double(imread('data/Grove3/frame10.png'))/255;
% I2 = double(imread('data/Grove3/frame11.png'))/255;
floPath = 'data/Amin/flow10.flo';

% call main routine
[flow] = coarse_to_fine(I1, I2, st, show_flow, h);

u = flow(:, :, 1);
v = flow(:, :, 2);

%% evalutate the correctness of the computed flow
% read the ground-truth flow
realFlow = readFlowFile(floPath);
tu = realFlow(:, :, 1);
tv = realFlow(:, :, 2);
% compute the mean end-point error (mepe) and the mean angular error (mang)
UNKNOWN_FLOW_THRESH = 1e9;
[mang, mepe] = flowError(tu, tv, u, v, ...
    0, 0.0, UNKNOWN_FLOW_THRESH);
disp(['Mean end-point error: ', num2str(mepe)]);
disp(['Mean angular error: ', num2str(mang)]);

% display the flow
flowImg = uint8(robust_flowToColor(flow));
figure; imshow(flowImg);
