/* 
 * Author: Marius Drulea
 * http://www.cv.utcluj.ro/optical-flow.html
 * 
 * Reference
 * Drulea, M.; Nedevschi, S., "Motion Estimation Using the Correlation Transform,"
 * Image Processing, IEEE Transactions on , vol.22, no.8, pp.3260,3270, Aug. 2013
 * 
 * Copyright (C) 2013 Technical University of Cluj-Napoca 
 */

#include "mex.h"

#include <algorithm>
#include <cmath>
using namespace std;

#include "Matrix.h"
#include "Matrix4.h"

/*
 * For each pixel, simply retain the intensities around it
 */
void compute_intensity_transform(Matrix4 C, Matrix I)
{            
    int dx = C.n_width;
    int dy = C.n_height;
    
    int dx2 = (dx - 1)/2;
    int dy2 = (dy - 1)/2;
	
    for (int j = 0; j < I.height; j++)
    {
        for (int i = 0; i < I.width; i++)
        {            			
			//store the intensities around the pixel (i, j)
			for (int y = -dy2; y <= dy2; y++)
            {                
                int jpy = j + y;
                if (jpy < 0) jpy = 0;                    
                if (jpy > I.height - 1) jpy = I.height - 1;                               
                
                for (int x = -dx2; x <= dx2; x++)
                {                    
                    int ipx = i + x;
                    if (ipx < 0) ipx = 0;
                    if (ipx > I.width - 1) ipx = I.width - 1;                    
                    
                    C(i, j, x+dx2, y+dy2) = I(ipx, jpy);
                }
			}
                        
        }
    }
    
}		
        
/* the gateway function */
void mexFunction( int nlhs, mxArray *plhs[],
        int nrhs, const mxArray *prhs[])
{
    if(nrhs != 2)
        mexErrMsgTxt("Two inputs required.");
    if(nlhs != 0)
        mexErrMsgTxt("No outputs required.");
    
    //get the inputs
    
    /*  get the dimensions of the matrix input data */
    //Warning: the dimension are inversed; width becames height after matlab's implicit transposition
    const int* dims_C = mxGetDimensions(prhs[0]);
    int width = (int) dims_C[0];
    int height = (int) dims_C[1];
    int dims_C_4_or_2 = (int) mxGetNumberOfDimensions(prhs[0]);
    int n_width = 1; //if the number of dimensions is 2, then n_width = 1;
    int n_height = 1;
    if (dims_C_4_or_2 == 4)
    {
        n_width = (int) dims_C[2];
        n_height = (int) dims_C[3];    
    }
    
    /*  create the input structures */
    Matrix4 C(width, height, n_width, n_height, (double*) mxGetPr(prhs[0]));
    Matrix I(width, height, (double*) mxGetPr(prhs[1]));
    //the function updates the inputs; no outputs required
    
    /*  call the C subroutine */
    compute_intensity_transform(C, I);
}