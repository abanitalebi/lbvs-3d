%
% Author: Marius Drulea
% http://www.cv.utcluj.ro/optical-flow.html

% Reference
% Drulea, M.; Nedevschi, S., "Motion Estimation Using the Correlation Transform,"
% Image Processing, IEEE Transactions on , vol.22, no.8, pp.3260,3270, Aug. 2013

% Copyright (C) 2013 Technical University of Cluj-Napoca

function [u, v, u_, v_, qu, qv] = solve_flow_one_level(u, v, u_, v_, qu, qv, ...
    I1c, I2c, I1c_for_bf, st, show_flow, h, level)

M = size(I1c, 1); N = size(I1c, 2);

% transform color images into grayscale images
% I1 = rgb2gray(I1c);% if data is double, the method assumes that the images are in [0, 1]
% I2 = rgb2gray(I2c);
I1 = 0.2989*I1c(:, :, 1) + 0.5870*I1c(:, :, 2) + 0.1140*I1c(:, :, 3);
I2 = 0.2989*I2c(:, :, 1) + 0.5870*I2c(:, :, 2) + 0.1140*I2c(:, :, 3);

dx = st.dx; dy = st.dy; nx = st.nx; ny = st.ny;

% settings for computing the bilateral coefficients
use_euclidian = 0; % 1 = use the euclidian distance between the colors or 0 = use the squared distance
if (use_euclidian == 1)
    % the euclidian version of bf works better for Schefflera and Teddy
    alpha = 5; % for color
    beta = 3; % for distance
else
    alpha = 2*5^2; % for color
    beta = 2*3^2; % for distance
end

% transform from RGB to Lab color space
I1_Lab = RGB2Lab(I1c_for_bf);

% compute the bilateral coefficients
% use_euclidian, normalization, boundary handling (0 -> the coefficient is 0 if the index is outside the valid range)
% computes_bilateral_filter(Lab_image, nx, ny, alpha, beta, use_euclidian, normalize_the_coefficients, boundary_handling_type)
% boundary_handling_type: 0 -> the coefficient is 0 if the index is outside the valid range
bf = compute_bilateral_filter(I1_Lab(:, :, 1), I1_Lab(:, :, 2), I1_Lab(:, :, 3), nx, ny, alpha, beta, use_euclidian, 0, 0);
% bf(:, :, (nx+1)/2, (ny+1)/2) = 1e-09;
bf = max(bf, 1e-09); % avoid further divisions by zero

% set the numbers of stencils and the step_size for computing the derivatives
step_size = 1; stencils = 3;

% the computation of the X-transform and it's spatial derivatives; X = {'correlation', 'census', 'intensity'}
[C1, C1x, C1y] = compute_X_transform_and_derivatives_warp(I1, dx, dy, zeros(M, N), zeros(M, N), ...
                                              st.data_term, st.warping_method, stencils, step_size);

% warping
for k = 1:st.warps
    % median filter helps to jump over local minima
    u0 = medfilt2(u_, [3 3], 'symmetric');
    v0 = medfilt2(v_, [3 3], 'symmetric');
    occ_condition = (level <= 2 && k >= 1); % apply occlusion refinement only on the finest two levels
    lambdas = st.lambda * ones(M, N);
    if (occ_condition == 1)
        % occlusion aware refinement, based on the uniqueness criterion; based on [12]        
        [occ ~] = occlusions_map_mex(u, v);
        occ2 = min(max(0, occ - 1), 1);
        occ_cost = max(1-occ2, 0.001);
        lambdas = st.lambda*ones(M, N).*occ_cost;
    end
    
    % warp the second image I2 with the flow (u0, v0) and compute the
    % correlation/census/intensity transform C2w of the warped image; also computes the
    % derivatives C2wx and C2wy; this implies two additional warpings for 3 stencils
    [C2w, C2wx, C2wy] = compute_X_transform_and_derivatives_warp(I2, dx, dy, u0, v0, ...
                                   st.data_term, st.warping_method, stencils, step_size);
    
    b = 0.5; % blending factor of the spatial derivarives
    Cx = b*C2wx + (1-b)*C1x;
    Cy = b*C2wy + (1-b)*C1y;
    
    Ct = C2w - C1; % the temporal derivatives
    
    % boundary handling (relies on the matlab function isnan)
    m1 = isnan(Cx);
    m2 = isnan(Cy);
    m3 = isnan(Ct);
    m = m1 | m2 | m3;
    Cx(m) = 0; Cy(m) = 0; Ct(m) = 0;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % find the solution of the convex problem using PPA (proximal-point
    % algorithm)
    [u, v, u_, v_, qu, qv] = solve_matching_nl_equation(u, v, u_, v_, qu, qv, u0, v0, ...
        Cx, Cy, Ct, bf, lambdas, st.max_its);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
%     if show_flow == 1
%         %%%%%%% display the occlusions even if they are not used for refinements
%         [occ ~] = occlusions_map_mex(u, v);
%         occ2 = min(max(0, occ - 1), 1);
%         occ_cost = 1-occ2;
%         occ_inds = find(occ_cost < 0.4);
%         I1c_occ = I1c;
%         aux = I1c(:, :, 1); aux(occ_inds) = 1; I1c_occ(:, :, 1) = aux;
%         aux = I1c(:, :, 2); aux(occ_inds) = 1; I1c_occ(:, :, 2) = aux;
%         aux = I1c(:, :, 3); aux(occ_inds) = 0; I1c_occ(:, :, 3) = aux;
%         set(0, 'CurrentFigure', h), subplot(2, 2, 1), imshow(I1c_occ);
%         title(['Occlusions over I1 at level ', int2str(level), ', warp ' int2str(k)]);
%         
%         %%%%%%% plot the warped image
%         [idx idy] = meshgrid(1:N, 1:M);                                
%         idxx = idx + u;
%         idyy = idy + v;
%         % boundary handling
%         m_out = (idxx > N) | (idxx < 1) | (idyy > M) | (idyy < 1);        
%         I2cw = zeros(M, N, 3);
%         for i = 1:3
%             I2cw_i = interp2(I2c(:, :, i), idxx, idyy, 'cubic');
%             I2cw_i(m_out) = 1;
%             I2cw(:, :, i) = I2cw_i;
%         end        
%         set(0,'CurrentFigure', h), subplot(2, 2, 3), imshow(I2cw);
%         title(['The warped image at level ', int2str(level), ', warp ' int2str(k)]);
%         
%         %%%%%%% plot the flow
%         flow(:, :, 1) = u; flow(:, :, 2) = v;
%         set(0,'CurrentFigure', h), subplot(2, 2, 4), imshow(uint8(robust_flowToColor(flow)));
%         title(['Flow at level ', int2str(level), ', warp ' int2str(k)]);   
%         
%         drawnow;
%     end
end