%
% Author: Marius Drulea
% http://www.cv.utcluj.ro/optical-flow.html

% Reference
% Drulea, M.; Nedevschi, S., "Motion Estimation Using the Correlation Transform," 
% Image Processing, IEEE Transactions on , vol.22, no.8, pp.3260,3270, Aug. 2013

% Copyright (C) 2013 Technical University of Cluj-Napoca

function [ st ] = load_settings( setting_type )

if strcmp(setting_type, 'correlation_basic')
    % the settings (st) of the method
    st.data_term = 'correlation'; % data_term ? {'correlation', 'census', 'intensity'}
    
    % data weighting and windows configurations
    st.lambda = 5; % the weight of the data term   ---> originally was 12
    st.dx = 3; st.dy = st.dx; % the size of the data window (the size of the correlation window, the size of the census window etc)
    st.nx = 3; st.ny = st.nx; % the size of non-local propagation window    --> originally was 5
    % rescalling settings
    st.pyramid_factor = 0.5;
    st.downsample_method = 'bilinear'; % the downsampling method used to build the pyramids
    st.upsample_method = 'bicubic'; % method for upsampling the flow
    st.downsample_method_for_bf = 'bilinear'; % the downsampling method for the pyramid used to compute the bilateral weights
    st.antialiasing_start_level = 3; % perform antialiasing for all the levels higher or equal than the given level; for the other levels do not use antialiasing
    % warping settings
    st.warps = 2; % the numbers of warps per level --> was '5' originally
    st.warping_method = 'cubic';
    % numerical scheme's settings
    st.max_its = 3; % the number of equation iterations per warp    --> was '10' originally
end

if strcmp(setting_type, 'correlation_full')
    % the settings (st) of the method
    st.data_term = 'correlation'; % data_term ? {'correlation', 'census', 'intensity'}
    
    % data weighting and windows configurations
    st.lambda = 17; % the weight of the data term
    st.dx = 3; st.dy = st.dx; % the size of the data window (the size of the correlation window, the size of the census window etc)
    st.nx = 7; st.ny = st.nx; % the size of non-local propagation window
    % rescalling settings
    st.pyramid_factor = 0.8;
    st.downsample_method = 'bilinear'; % the downsampling method used to build the pyramids
    st.upsample_method = 'bicubic'; % method for upsampling the flow
    st.downsample_method_for_bf = 'bilinear'; % the downsampling method for the pyramid used to compute the bilateral weights
    st.antialiasing_start_level = 4; % perform antialiasing for all the levels higher or equal than the given level; for the other levels do not use antialiasing
    % warping settings
    st.warps = 8; % the numbers of warps per level
    st.warping_method = 'cubic';
    % numerical scheme's settings
    st.max_its = 32; % the number of equation iterations per warp
end

if strcmp(setting_type, 'census_basic') % the parameters and the boundary handling for this data term are not optimized
    % the settings (st) of the method
    st.data_term = 'census'; % data_term ? {'correlation', 'census', 'intensity'}
    
    % data weighting and windows configurations
    st.lambda = 5; % the weight of the data term
    st.dx = 3; st.dy = st.dx; % the size of the data window (the size of the correlation window, the size of the census window etc)
    st.nx = 5; st.ny = st.nx; % the size of non-local propagation window
    % rescalling settings
    st.pyramid_factor = 0.5;
    st.downsample_method = 'bilinear'; % the downsampling method used to build the pyramids
    st.upsample_method = 'bicubic'; % method for upsampling the flow
    st.downsample_method_for_bf = 'bilinear'; % the downsampling method for the pyramid used to compute the bilateral weights
    st.antialiasing_start_level = 3; % perform antialiasing for all the levels higher or equal than the given level; for the other levels do not use antialiasing
    % warping settings
    st.warps = 5; % the numbers of warps per level
    st.warping_method = 'cubic';
    % numerical scheme's settings
    st.max_its = 10; % the number of equation iterations per warp
end

if strcmp(setting_type, 'intensity_basic') % the parameters and the boundary handling for this data term are not optimized
    % the settings (st) of the method
    st.data_term = 'intensity'; % data_term ? {'correlation', 'census', 'intensity'}
    
    % data weighting and windows configurations
    st.lambda = 5000; % the weight of the data term
    st.dx = 3; st.dy = st.dx; % the size of the data window (the size of the correlation window, the size of the census window etc)
    st.nx = 5; st.ny = st.nx; % the size of non-local propagation window
    % rescalling settings
    st.pyramid_factor = 0.5;
    st.downsample_method = 'bilinear'; % the downsampling method used to build the pyramids
    st.upsample_method = 'bicubic'; % method for upsampling the flow
    st.downsample_method_for_bf = 'bilinear'; % the downsampling method for the pyramid used to compute the bilateral weights
    st.antialiasing_start_level = 3; % perform antialiasing for all the levels higher or equal than the given level; for the other levels do not use antialiasing
    % warping settings
    st.warps = 5; % the numbers of warps per level
    st.warping_method = 'cubic';
    % numerical scheme's settings
    st.max_its = 10; % the number of equation iterations per warp
end

end

