%
% Author: Marius Drulea
% http://www.cv.utcluj.ro/optical-flow.html

% Reference
% Drulea, M.; Nedevschi, S., "Motion Estimation Using the Correlation Transform,"
% Image Processing, IEEE Transactions on , vol.22, no.8, pp.3260,3270, Aug. 2013

% Copyright (C) 2013 Technical University of Cluj-Napoca

function [ Cw, Cwx, Cwy ] = compute_X_transform_and_derivatives_warp( I, dx, dy, u0, v0, data_term, interp_method, stencils, step_size)

M = size(I, 1); N = size(I, 2);

[idx idy] = meshgrid(1:N, 1:M);

% the step size used in the computation of the derivatives
s = step_size;

% create a function handle for each data term X in {'correlation', 'census', 'intensity'}
if strcmp(data_term, 'correlation')
    X_transform = @correlation_transform_mex;
end
if strcmp(data_term, 'census')
    X_transform = @census_transform_mex;
end
if strcmp(data_term, 'intensity')
    X_transform = @intensity_transform_mex;
end

Cw = zeros(M, N, dx, dy);
% warp the image with the flow (u0, v0) and compute the correlation
% transform of the warped image
idxx = idx + u0;
idyy = idy + v0;
Iw = interp2(I, idxx, idyy, interp_method);
X_transform(Cw, Iw); % correlation_transform_mex(Cw, Iw);

% warps and compute the correlations necessary for spatial derivatives
% +- s
Cw_ups = zeros(M, N, dx, dy);
Cw_ums = zeros(M, N, dx, dy);
Cw_vps = zeros(M, N, dx, dy);
Cw_vms = zeros(M, N, dx, dy);

idxx = idx + u0 + s;
idyy = idy + v0;
Iw_ups = interp2(I, idxx, idyy, interp_method);

idxx = idx + u0 - s;
idyy = idy + v0;
Iw_ums = interp2(I, idxx, idyy, interp_method);

idxx = idx + u0;
idyy = idy + v0 + s;
Iw_vps = interp2(I, idxx, idyy, interp_method);

idxx = idx + u0;
idyy = idy + v0 - s;
Iw_vms = interp2(I, idxx, idyy, interp_method);

X_transform(Cw_ups, Iw_ups); % correlation_transform_mex(Cw_ups, Iw_ups);
X_transform(Cw_ums, Iw_ums); % correlation_transform_mex(Cw_ums, Iw_ums);
X_transform(Cw_vps, Iw_vps); % correlation_transform_mex(Cw_vps, Iw_vps);
X_transform(Cw_vms, Iw_vms); % correlation_transform_mex(Cw_vms, Iw_vms);

if (stencils == 5)
    % +- 2*s
    Cw_up2s = zeros(M, N, dx, dy);
    Cw_um2s = zeros(M, N, dx, dy);
    Cw_vp2s = zeros(M, N, dx, dy);
    Cw_vm2s = zeros(M, N, dx, dy);
    
    idxx = idx + u0 + 2*s;
    idyy = idy + v0;    
    Iw_up2s = interp2(I, idxx, idyy, interp_method);
    
    idxx = idx + u0 - 2*s;
    idyy = idy + v0;    
    Iw_um2s = interp2(I, idxx, idyy, interp_method);
    
    idxx = idx + u0;
    idyy = idy + v0 + 2*s;    
    Iw_vp2s = interp2(I, idxx, idyy, interp_method);
    
    idxx = idx + u0;
    idyy = idy + v0 - 2*s;
    Iw_vm2s = interp2(I, idxx, idyy, interp_method);
    
    X_transform(Cw_up2s, Iw_up2s); % correlation_transform_mex(Cw_up2s, Iw_up2s);
    X_transform(Cw_um2s, Iw_um2s); % correlation_transform_mex(Cw_um2s, Iw_um2s);
    X_transform(Cw_vp2s, Iw_vp2s); % correlation_transform_mex(Cw_vp2s, Iw_vp2s);
    X_transform(Cw_vm2s, Iw_vm2s); % correlation_transform_mex(Cw_vm2s, Iw_vm2s);
end

% compute the spatial derivatives
if (stencils == 3)
    Cwx = (Cw_ups - Cw_ums)/(2*s);
    Cwy = (Cw_vps - Cw_vms)/(2*s);
end

if (stencils == 5)
    Cwx = (Cw_um2s - 8*Cw_ums + 8*Cw_ups - Cw_up2s)/(12*s);
    Cwy = (Cw_vm2s - 8*Cw_vms + 8*Cw_vps - Cw_vp2s)/(12*s);
end

end

