%
% Author: Marius Drulea
% http://www.cv.utcluj.ro/optical-flow.html

% References
% Drulea, M.; Nedevschi, S., "Motion Estimation Using the Correlation Transform,"
% Image Processing, IEEE Transactions on , vol.22, no.8, pp.3260,3270, Aug. 2013

% Copyright (C) 2013 Technical University of Cluj-Napoca

function [flow] = coarse_to_fine(I1, I2, st, show_flow, h)

    M = size(I1, 1); N = size(I1, 2);

    % computes the maximum number of pyramid levels; the coarsest image should
    % have a width or height around 10
    pyramid_levels = min(...
        ceil(log(10/M)/log(st.pyramid_factor)), ...
        ceil(log(10/N)/log(st.pyramid_factor)));

    pyrI1 = cell(pyramid_levels, 1);
    pyrI2 = cell(pyramid_levels, 1);
    pyrI1_for_bf = cell(pyramid_levels, 1);

    pyrI1{1} = I1;
    pyrI2{1} = I2;
    pyrI1_for_bf{1} = I1;

    % build the pyramids
    for i = 2:pyramid_levels
        pyrI1{i} = imresize(I1, (st.pyramid_factor)^(i-1), st.downsample_method);
        pyrI2{i} = imresize(I2, (st.pyramid_factor)^(i-1), st.downsample_method);

        % separate downsampling for the pyramid used for the bilateral coefficients
        % use or not antialiasing for the current level
        if i >= st.antialiasing_start_level
            antialiasing = true;
        else
            antialiasing = false;
        end
        pyrI1_for_bf{i} = imresize(I1, (st.pyramid_factor)^(i-1), st.downsample_method_for_bf, 'Antialiasing', antialiasing);
    end

    % start coarse to fine processing
    for level = pyramid_levels:-1:1;

        M = size(pyrI1{level}, 1);
        N = size(pyrI1{level}, 2);

        if level == pyramid_levels % if the coarsest level
            % initialization of the flow
            u = zeros(M,N);
            v = zeros(M,N);
            u_ = zeros(M,N);
            v_ = zeros(M,N);
            % initialization of the dual variables
            qu = zeros(M, N, st.nx, st.ny);
            qv = zeros(M, N, st.nx, st.ny);
        else % if not the coarsest level, upsample the flow from the previous level to the current level
            rescale_factor_u = N/size(pyrI1{level+1}, 2); % rescaling factor along width
            rescale_factor_v = M/size(pyrI1{level+1}, 1); % rescaling factor along height

            % prolongate to finer grid
            u = imresize(u, [M N], st.upsample_method)*rescale_factor_u;
            v = imresize(v, [M N], st.upsample_method)*rescale_factor_v;

            u_ = imresize(u_, [M N], st.upsample_method)*rescale_factor_u;
            v_ = imresize(v_, [M N], st.upsample_method)*rescale_factor_v;

            % also prolongate the dual variables
            qu_tmp = qu;
            qv_tmp = qv;

            qu = zeros(M, N, st.nx, st.ny);
            qv = zeros(M, N, st.nx, st.ny);

            for i=1:size(qu, 3)
                for j=1:size(qu, 4)
                    qu(:,:,i, j) = imresize(qu_tmp(:,:,i,j), [M N], st.upsample_method);
                    qv(:,:,i, j) = imresize(qv_tmp(:,:,i,j), [M N], st.upsample_method);
                end
            end

        end

        if show_flow == 1
            set(0,'CurrentFigure', h);
            subplot(2,2,1), imshow(pyrI1{level}), title('I1');
            subplot(2,2,2), imshow(pyrI2{level}), title('I2');
            drawnow;
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % solve the flow on the current level
        [u, v, u_, v_, qu, qv] = solve_flow_one_level(u, v, u_, v_,qu, qv, ...
            pyrI1{level}, pyrI2{level}, pyrI1_for_bf{level}, st, show_flow, h, level);    
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    end

    % store the final flow
    flow = zeros(M,N,2);
    flow(:,:,1) = u;
    flow(:,:,2) = v;
end