function [max_values_wta, max_y_wta, max_x_wta] = winner_take_all(saliency)
% This function computes the Winner-Take-All (WTA) algorithm

    num_max_thresh = 4; %100;  % threshold of number of maximum
    ratio_max_thresh = 1.6;    % threshold of ratio between two consecutive maximum
    neighborhood_radius = 100; % radius of a circle that is considered as the spatial neighborhood
    neighborhood_thresh = neighborhood_radius^2; 

    size_saliency = size(saliency); % store the size of the saliency map, a vector of 1*2 
    [X,Y] = meshgrid(1:size_saliency(2), 1:size_saliency(1));
    min_value = min(saliency(:)); % global minimum value
    L = max(saliency(:))-min_value; % maximum dynamic
    alpha = (max(saliency(:))+min_value)/2.3; %(L-0.1)/2; % threshold to decide whether a local maximum is relevent

    max_value = zeros(num_max_thresh, 1); % assign a vector to store the maximum values
    max_ind = zeros(num_max_thresh, 1); % assign a vector to store the index of maximum values
    for num_max = 1 : num_max_thresh
        % find the global maximum
        [max_value(num_max), max_ind(num_max)] = max(saliency(:));
        % for loop termination conditions
        if num_max > 1
            if max_value(num_max-1)/max_value(num_max) > ratio_max_thresh || max_value(num_max) < alpha 
                num_max = num_max - 1; % the current maximum does not count
                break;
            end
        end
        % inhibit the spatial neighborhood of the currently found global maximum
        [y_max, x_max] = ind2sub(size_saliency, max_ind(num_max));
        saliency( (X(:)-x_max).^2+(Y(:)-y_max).^2 < neighborhood_thresh ) = min_value;
    end
    % store the values and locations of the maximum
    max_values_wta = max_value(1:num_max);
    [max_y_wta, max_x_wta] = ind2sub(size_saliency, max_ind(1:num_max));

end