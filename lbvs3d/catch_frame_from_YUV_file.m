function frame = catch_frame_from_YUV_file(path , f , width, height, format)
    
fwidth = 0.5;
fheight= 0.5;
if strcmp(format,'400')
    fwidth = 0;
    fheight= 0;
elseif strcmp(format,'411')
    fwidth = 0.25;
    fheight= 1;
elseif strcmp(format,'420')
    fwidth = 0.5;
    fheight= 0.5;
elseif strcmp(format,'422')
    fwidth = 0.5;
    fheight= 1;
elseif strcmp(format,'444')
    fwidth = 1;
    fheight= 1;
else
    display('Error: wrong format');
end

YUV = loadFileYUV(width,height,f,path,fheight,fwidth);
RGB = ycbcr2rgb(YUV); 
frame = RGB;

end