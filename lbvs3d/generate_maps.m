tic
% This is to generate saliency maps

clear all; close all; clc;
warning off all

% Create a parallel MATLAB Pool (version 2014a)
% delete(gcp);
% parpool(6);

% % Create a parallel MATLAB pool (version 2012b)
% matlabpool('open',4);
% matlabpool('close');      % This goes at the end of the program

input_video_folder_path = 'H:\PhD\Documents\3DSaliency\2Datasets\UBCeye\Dataset\1_YUV_disparity_corrected';
maps_folder_path = strcat(input_video_folder_path, '\Maps');
         
nof_max = 1;                   % number of frames for which you would like to generate the saliency map. min(nof_max, total frames in the file) will be used.
width = 1920;
height = 1080;
format = '420';
fovea_radius = 73;      % This corresponds to 2*alpha=2 degrees visual angle. Has to be an odd number. It is defined based on the display size and distance of the viewer. Default is 73 for HD TV.
sp=7;%spatial domain threshold
col=6.5;% color threshold
reg=50; %region size
FR = 30;
display_width_cm = 101.84;
resize_factor = 4;      % resize_factor = 1 will use the original video resolution. It makes the code slwo.
dims = round([height, width]/resize_factor); 
fovea_radius = floor(fovea_radius/(2*resize_factor))*2+1;

disparity_range = xlsread('DepthSearchRange3.xlsx');    % Left-to-right disparity using DERS gives you the disparity map corresponding to the right view
disparity_range_min = disparity_range(:,2);
disparity_range_max = disparity_range(:,1);

addpath('edison_matlab_interface');         addpath('Correlation Flow');        addpath('voc-release-3.1-win-master');      addpath('LabelMeToolbox');      addpath(genpath('optprop'));

dirOutput = dir(fullfile(input_video_folder_path,'*.yuv'));
fileNames = {dirOutput.name}';
numFiles = numel(fileNames);
fovea_mask = fovea_mask(fovea_radius);

for f=1:numFiles
        
    YUV_video_name_l = strcat(input_video_folder_path,'\',fileNames{f});
    intensity_map_name = strcat(maps_folder_path, '\', fileNames{f});        intensity_map_name = strcat(intensity_map_name(1:end-4), '_brightness', '.yuv');
    color_map_name1 = strcat(maps_folder_path, '\', fileNames{f});        color_map_name1 = strcat(color_map_name1(1:end-4), '_color1', '.yuv');
    color_map_name2 = strcat(maps_folder_path, '\', fileNames{f});        color_map_name2 = strcat(color_map_name2(1:end-4), '_color2', '.yuv');
    color_map_name3 = strcat(maps_folder_path, '\', fileNames{f});        color_map_name3 = strcat(color_map_name3(1:end-4), '_color3', '.yuv');
    color_map_name4 = strcat(maps_folder_path, '\', fileNames{f});        color_map_name4 = strcat(color_map_name4(1:end-4), '_color4', '.yuv');
    color_map_name5 = strcat(maps_folder_path, '\', fileNames{f});        color_map_name5 = strcat(color_map_name5(1:end-4), '_color5', '.yuv');
    color_map_name6 = strcat(maps_folder_path, '\', fileNames{f});        color_map_name6 = strcat(color_map_name6(1:end-4), '_color6', '.yuv');
    color_map_name7 = strcat(maps_folder_path, '\', fileNames{f});        color_map_name7 = strcat(color_map_name7(1:end-4), '_color7', '.yuv');
    texture_map_name = strcat(maps_folder_path, '\', fileNames{f});        texture_map_name = strcat(texture_map_name(1:end-4), '_texture', '.yuv');
    motion_map_name1 = strcat(maps_folder_path, '\', fileNames{f});        motion_map_name1 = strcat(motion_map_name1(1:end-4), '_motion1', '.yuv');
    motion_map_name2 = strcat(maps_folder_path, '\', fileNames{f});        motion_map_name2 = strcat(motion_map_name2(1:end-4), '_motion2', '.yuv');
    motion_map_name3 = strcat(maps_folder_path, '\', fileNames{f});        motion_map_name3 = strcat(motion_map_name3(1:end-4), '_motion3', '.yuv');
    motion_map_name4 = strcat(maps_folder_path, '\', fileNames{f});        motion_map_name4 = strcat(motion_map_name4(1:end-4), '_motion4', '.yuv');
    motion_map_name5 = strcat(maps_folder_path, '\', fileNames{f});        motion_map_name5 = strcat(motion_map_name5(1:end-4), '_motion5', '.yuv');
    motion_map_name6 = strcat(maps_folder_path, '\', fileNames{f});        motion_map_name6 = strcat(motion_map_name6(1:end-4), '_motion6', '.yuv');
    motion_map_name7 = strcat(maps_folder_path, '\', fileNames{f});        motion_map_name7 = strcat(motion_map_name7(1:end-4), '_motion7', '.yuv');
    depth_feature_map_name1 = strcat(maps_folder_path, '\', fileNames{f});        depth_feature_map_name1 = strcat(depth_feature_map_name1(1:end-4), '_depth_feature1', '.yuv');
    discomfort_map_name = strcat(maps_folder_path, '\', fileNames{f});        discomfort_map_name = strcat(discomfort_map_name(1:end-4), '_discomfort', '.yuv');
    person_map_name = strcat(maps_folder_path, '\', fileNames{f});        person_map_name = strcat(person_map_name(1:end-4), '_person', '.yuv');
    vehicle_map_name = strcat(maps_folder_path, '\', fileNames{f});        vehicle_map_name = strcat(vehicle_map_name(1:end-4), '_vehicle', '.yuv');
    animal_map_name = strcat(maps_folder_path, '\', fileNames{f});        animal_map_name = strcat(animal_map_name(1:end-4), '_animal', '.yuv');
    face_map_name = strcat(maps_folder_path, '\', fileNames{f});        face_map_name = strcat(face_map_name(1:end-4), '_face', '.yuv');
    text_map_name = strcat(maps_folder_path, '\', fileNames{f});        text_map_name = strcat(text_map_name(1:end-4), '_text', '.yuv');
    horizon_map_name = strcat(maps_folder_path, '\', fileNames{f});        horizon_map_name = strcat(horizon_map_name(1:end-4), '_horizon', '.yuv');
    
        
    if YUV_video_name_l(88) == 'l'
        num_frames_yuv = yuv_frame_number(YUV_video_name_l,width,height,format);
        nof = min(num_frames_yuv, nof_max);
        YUV_video_name_r = YUV_video_name_l;      YUV_video_name_r(88) = 'r';
        YUV_video_name_D = YUV_video_name_l;      YUV_video_name_D(88) = 'D';
     
        % Conspicuty maps
        for i = 1:nof   %parfor
            frame_L = catch_frame_from_YUV_file(YUV_video_name_l, i , width, height, format);
            frame_R = catch_frame_from_YUV_file(YUV_video_name_r, i , width, height, format);
            frame_D = catch_frame_from_YUV_file(YUV_video_name_D, i , width, height, format);
            
            frame_L = imresize(frame_L, dims);
            frame_R = imresize(frame_R, dims);
            frame_D = imresize(frame_D, dims);
            
            frame_yuv_L = rgb2ycbcr(frame_L);
            frame_yuv_R = rgb2ycbcr(frame_R);
            frame_Lab_R = RGB2Lab(frame_R);
            frame_DERS = double(rgb2gray(frame_D));     frame_DERS = medfilt2(frame_DERS, [5,5]);
            min_d_range = disparity_range_min(f);   max_d_range = disparity_range_max(f);   
            disparity_map = round(((frame_DERS-min(min(frame_DERS)))/(max(max(frame_DERS))-min(min(frame_DERS)))) * (max_d_range-min_d_range) + min_d_range);
            depth_map = disparity2depth(disparity_map);
            
            % Segmentation
            [fimg labels modes segsize_vector grad conf] = edison_wrapper(frame_R,@RGB2Luv,'SpatialBandWidth',sp,'RangeBandWidth',col,'MinimumRegionArea',reg);
            labels = double(labels + 1);    
            
            % brightness intensity saliency map
            temp1 = brightness_map_calculator(frame_yuv_R, labels, fovea_radius, fovea_mask);
            temp1 = imresize(temp1, [height, width]);   temp1 = temp1 / max(temp1(:));
            map_brightness(i).cdata(:,:,1) = uint8(255*temp1);   map_brightness(i).cdata(:,:,2) = uint8(255*temp1);   map_brightness(i).cdata(:,:,3) = uint8(255*temp1);   
            map_brightness(i).colormap = [];
            
%             % Color saliency map
            temp2 = color_map_calculator(frame_Lab_R, frame_R, labels, fovea_radius, fovea_mask);   
            temp2_1 = temp2(:,:,1); temp2_1 = imresize(temp2_1, [height, width]);   temp2_1 = temp2_1 / max(temp2_1(:));
            temp2_2 = temp2(:,:,2); temp2_2 = imresize(temp2_2, [height, width]);   temp2_2 = temp2_2 / max(temp2_2(:));
            temp2_3 = temp2(:,:,3); temp2_3 = imresize(temp2_3, [height, width]);   temp2_3 = temp2_3 / max(temp2_3(:));
            temp2_4 = temp2(:,:,4); temp2_4 = imresize(temp2_4, [height, width]);   temp2_4 = temp2_4 / max(temp2_4(:));
            temp2_5 = temp2(:,:,5); temp2_5 = imresize(temp2_5, [height, width]);   temp2_5 = temp2_5 / max(temp2_5(:));
            temp2_6 = temp2(:,:,6); temp2_6 = imresize(temp2_6, [height, width]);   temp2_6 = temp2_6 / max(temp2_6(:));
            temp2_7 = temp2(:,:,7); temp2_7 = imresize(temp2_7, [height, width]);   temp2_7 = temp2_7 / max(temp2_7(:));
            
            map_color_1(i).cdata(:,:,1) = uint8(255*temp2_1);   map_color_1(i).cdata(:,:,2) = uint8(255*temp2_1);   map_color_1(i).cdata(:,:,3) = uint8(255*temp2_1);   map_color_1(i).colormap = [];
            map_color_2(i).cdata(:,:,1) = uint8(255*temp2_2);   map_color_2(i).cdata(:,:,2) = uint8(255*temp2_2);   map_color_2(i).cdata(:,:,3) = uint8(255*temp2_2);   map_color_2(i).colormap = [];
            map_color_3(i).cdata(:,:,1) = uint8(255*temp2_3);   map_color_3(i).cdata(:,:,2) = uint8(255*temp2_3);   map_color_3(i).cdata(:,:,3) = uint8(255*temp2_3);   map_color_3(i).colormap = [];
            map_color_4(i).cdata(:,:,1) = uint8(255*temp2_4);   map_color_4(i).cdata(:,:,2) = uint8(255*temp2_4);   map_color_4(i).cdata(:,:,3) = uint8(255*temp2_4);   map_color_4(i).colormap = [];
            map_color_5(i).cdata(:,:,1) = uint8(255*temp2_5);   map_color_5(i).cdata(:,:,2) = uint8(255*temp2_5);   map_color_5(i).cdata(:,:,3) = uint8(255*temp2_5);   map_color_5(i).colormap = [];
            map_color_6(i).cdata(:,:,1) = uint8(255*temp2_6);   map_color_6(i).cdata(:,:,2) = uint8(255*temp2_6);   map_color_6(i).cdata(:,:,3) = uint8(255*temp2_6);   map_color_6(i).colormap = [];
            map_color_7(i).cdata(:,:,1) = uint8(255*temp2_7);   map_color_7(i).cdata(:,:,2) = uint8(255*temp2_7);   map_color_7(i).cdata(:,:,3) = uint8(255*temp2_7);   map_color_7(i).colormap = [];          
            
            % Texture & Orientation maps
            temp3 = texture_map_calculator(frame_yuv_R, labels, fovea_radius, fovea_mask, grad);
            temp3 = imresize(temp3, [height, width]);   temp3 = temp3 / max(temp3(:));
            map_texture(i).cdata(:,:,1) = uint8(255*temp3);   map_texture(i).cdata(:,:,2) = uint8(255*temp3);   map_texture(i).cdata(:,:,3) = uint8(255*temp3);   map_texture(i).colormap = [];
            
            % Motion Maps
            ref_frame_R = double(frame_R)/255;      ref_disparity_map = disparity_map;
            if i < num_frames_yuv-1
                current_frame_R = double(catch_frame_from_YUV_file(YUV_video_name_r, i+1 , width, height, format))/255;            
                current_DERS_frame = double(catch_frame_from_YUV_file(YUV_video_name_D, i+1 , width, height, format))/255;           current_DERS_frame = double(medfilt2(rgb2gray(current_DERS_frame), [3,3]));
%                 current_disparity_map = round(((current_DERS_frame-min(min(current_DERS_frame)))/(max(max(current_DERS_frame))-min(min(current_DERS_frame)))) * (max_d_range-min_d_range) + min_d_range);
                next_frame_R = double(catch_frame_from_YUV_file(YUV_video_name_r, i+2 , width, height, format))/255;            
                next_DERS_frame = double(catch_frame_from_YUV_file(YUV_video_name_D, i+2 , width, height, format))/255;           next_DERS_frame = double(medfilt2(rgb2gray(next_DERS_frame), [3,3]));
%                 next_disparity_map = round(((next_DERS_frame-min(min(next_DERS_frame)))/(max(max(next_DERS_frame))-min(min(next_DERS_frame)))) * (max_d_range-min_d_range) + min_d_range);
                last_frame_flag = 0;
                %resize them to dims
                current_frame_R = imresize(current_frame_R, dims);   current_frame_R = current_frame_R / max(current_frame_R(:));
                current_DERS_frame = imresize(current_DERS_frame, dims);   current_DERS_frame = current_DERS_frame / max(current_DERS_frame(:));
                current_disparity_map = round(((current_DERS_frame-min(min(current_DERS_frame)))/(max(max(current_DERS_frame))-min(min(current_DERS_frame)))) * (max_d_range-min_d_range) + min_d_range);
                next_frame_R = imresize(next_frame_R, dims);   next_frame_R = next_frame_R / max(next_frame_R(:));
                next_DERS_frame = imresize(next_DERS_frame, dims);   next_DERS_frame = next_DERS_frame / max(next_DERS_frame(:));
                next_disparity_map = round(((next_DERS_frame-min(min(next_DERS_frame)))/(max(max(next_DERS_frame))-min(min(next_DERS_frame)))) * (max_d_range-min_d_range) + min_d_range);
                
            else
                current_frame_R = double(catch_frame_from_YUV_file(YUV_video_name_r, i-1 , width, height, format))/255;             
                current_DERS_frame = double(catch_frame_from_YUV_file(YUV_video_name_D, i-1 , width, height, format))/255;           current_DERS_frame = double(medfilt2(rgb2gray(current_DERS_frame), [3,3]));
%                 current_disparity_map = round(((current_DERS_frame-min(min(current_DERS_frame)))/(max(max(current_DERS_frame))-min(min(current_DERS_frame)))) * (max_d_range-min_d_range) + min_d_range);
                next_frame_R = double(catch_frame_from_YUV_file(YUV_video_name_r, i-2 , width, height, format))/255;             
                next_DERS_frame = double(catch_frame_from_YUV_file(YUV_video_name_D, i-2 , width, height, format))/255;           next_DERS_frame = double(medfilt2(rgb2gray(next_DERS_frame), [3,3]));
%                 next_disparity_map = round(((next_DERS_frame-min(min(next_DERS_frame)))/(max(max(next_DERS_frame))-min(min(next_DERS_frame)))) * (max_d_range-min_d_range) + min_d_range);
                last_frame_flag = 1;
                %resize them to dims
                current_frame_R = imresize(current_frame_R, dims);   current_frame_R = current_frame_R / max(current_frame_R(:));
                current_DERS_frame = imresize(current_DERS_frame, dims);   current_DERS_frame = current_DERS_frame / max(current_DERS_frame(:));
                current_disparity_map = round(((current_DERS_frame-min(min(current_DERS_frame)))/(max(max(current_DERS_frame))-min(min(current_DERS_frame)))) * (max_d_range-min_d_range) + min_d_range);
                next_frame_R = imresize(next_frame_R, dims);   next_frame_R = next_frame_R / max(next_frame_R(:));
                next_DERS_frame = imresize(next_DERS_frame, dims);   next_DERS_frame = next_DERS_frame / max(next_DERS_frame(:));
                next_disparity_map = round(((next_DERS_frame-min(min(next_DERS_frame)))/(max(max(next_DERS_frame))-min(min(next_DERS_frame)))) * (max_d_range-min_d_range) + min_d_range);
                
            end
            temp4 = motion_map_calculator(ref_frame_R, current_frame_R, next_frame_R, ref_disparity_map, current_disparity_map, next_disparity_map, last_frame_flag, labels, display_width_cm, FR);
            temp4_1 = temp4(:,:,1); temp4_1 = imresize(temp4_1, [height, width]);   temp4_1 = temp4_1 / max(temp4_1(:));
            temp4_2 = temp4(:,:,2); temp4_2 = imresize(temp4_2, [height, width]);   temp4_2 = temp4_2 / max(temp4_2(:));
            temp4_3 = temp4(:,:,3); temp4_3 = imresize(temp4_3, [height, width]);   temp4_3 = temp4_3 / max(temp4_3(:));
            temp4_4 = temp4(:,:,4); temp4_4 = imresize(temp4_4, [height, width]);   temp4_4 = temp4_4 / max(temp4_4(:));
            temp4_5 = temp4(:,:,5); temp4_5 = imresize(temp4_5, [height, width]);   temp4_5 = temp4_5 / max(temp4_5(:));
            temp4_6 = temp4(:,:,6); temp4_6 = imresize(temp4_6, [height, width]);   temp4_6 = temp4_6 / max(temp4_6(:));
            temp4_7 = temp4(:,:,7); temp4_7 = imresize(temp4_7, [height, width]);   temp4_7 = temp4_7 / max(temp4_7(:));
            
            map_motion_1(i).cdata(:,:,1) = uint8(255*temp4_1);   map_motion_1(i).cdata(:,:,2) = uint8(255*temp4_1);   map_motion_1(i).cdata(:,:,3) = uint8(255*temp4_1);   map_motion_1(i).colormap = [];
            map_motion_2(i).cdata(:,:,1) = uint8(255*temp4_2);   map_motion_2(i).cdata(:,:,2) = uint8(255*temp4_2);   map_motion_2(i).cdata(:,:,3) = uint8(255*temp4_2);   map_motion_2(i).colormap = [];
            map_motion_3(i).cdata(:,:,1) = uint8(255*temp4_3);   map_motion_3(i).cdata(:,:,2) = uint8(255*temp4_3);   map_motion_3(i).cdata(:,:,3) = uint8(255*temp4_3);   map_motion_3(i).colormap = [];
            map_motion_4(i).cdata(:,:,1) = uint8(255*temp4_4);   map_motion_4(i).cdata(:,:,2) = uint8(255*temp4_4);   map_motion_4(i).cdata(:,:,3) = uint8(255*temp4_4);   map_motion_4(i).colormap = [];
            map_motion_5(i).cdata(:,:,1) = uint8(255*temp4_5);   map_motion_5(i).cdata(:,:,2) = uint8(255*temp4_5);   map_motion_5(i).cdata(:,:,3) = uint8(255*temp4_5);   map_motion_5(i).colormap = [];
            map_motion_6(i).cdata(:,:,1) = uint8(255*temp4_6);   map_motion_6(i).cdata(:,:,2) = uint8(255*temp4_6);   map_motion_6(i).cdata(:,:,3) = uint8(255*temp4_6);   map_motion_6(i).colormap = [];
            map_motion_7(i).cdata(:,:,1) = uint8(255*temp4_7);   map_motion_7(i).cdata(:,:,2) = uint8(255*temp4_7);   map_motion_7(i).cdata(:,:,3) = uint8(255*temp4_7);   map_motion_7(i).colormap = [];          
            
            % Depth maps
            temp5 = depth_feature_map_calculator(depth_map, labels, resize_factor);
            temp5 = imresize(temp5, [height, width]);   temp5 = temp5 / max(temp5(:));
            map_depth_1(i).cdata(:,:,1) = uint8(255*temp5);   map_depth_1(i).cdata(:,:,2) = uint8(255*temp5);   map_depth_1(i).cdata(:,:,3) = uint8(255*temp5);   map_depth_1(i).depthmap = [];            
            
            % Visual Discomfort Penalizing Map
            temp6 = discomfort_penalizing_map(disparity_map, labels);
            temp6 = imresize(temp6, [height, width]);   temp6 = temp6 / max(temp6(:));
            map_discomfort(i).cdata(:,:,1) = uint8(255*temp6);   map_discomfort(i).cdata(:,:,2) = uint8(255*temp6);   map_discomfort(i).cdata(:,:,3) = uint8(255*temp6);   map_discomfort(i).depthmap = [];            
            
            % Top-bottom map
            [person_map, vehicle_map, animal_map, face_map, text_map, horizon_map] = top_bottom_maps(frame_R);
            person_map = imresize(person_map, [height, width]);   person_map = person_map / max(person_map(:));
            vehicle_map = imresize(vehicle_map, [height, width]);   vehicle_map = vehicle_map / max(vehicle_map(:));
            animal_map = imresize(animal_map, [height, width]);   animal_map = animal_map / max(animal_map(:));
            face_map = imresize(face_map, [height, width]);   face_map = face_map / max(face_map(:));
            text_map = imresize(text_map, [height, width]);   text_map = text_map / max(text_map(:));
            horizon_map = imresize(horizon_map, [height, width]);   horizon_map = horizon_map / max(horizon_map(:));
            
            map_person(i).cdata(:,:,1) = uint8(255*person_map);   map_person(i).cdata(:,:,2) = uint8(255*person_map);   map_person(i).cdata(:,:,3) = uint8(255*person_map);   map_person(i).depthmap = [];            
            map_vehicle(i).cdata(:,:,1) = uint8(255*vehicle_map);   map_vehicle(i).cdata(:,:,2) = uint8(255*vehicle_map);   map_vehicle(i).cdata(:,:,3) = uint8(255*vehicle_map);   map_vehicle(i).depthmap = [];            
            map_animal(i).cdata(:,:,1) = uint8(255*animal_map);   map_animal(i).cdata(:,:,2) = uint8(255*animal_map);   map_animal(i).cdata(:,:,3) = uint8(255*animal_map);   map_animal(i).depthmap = [];            
            map_face(i).cdata(:,:,1) = uint8(255*face_map);   map_face(i).cdata(:,:,2) = uint8(255*face_map);   map_face(i).cdata(:,:,3) = uint8(255*face_map);   map_face(i).depthmap = [];            
            map_text(i).cdata(:,:,1) = uint8(255*text_map);   map_text(i).cdata(:,:,2) = uint8(255*text_map);   map_text(i).cdata(:,:,3) = uint8(255*text_map);   map_text(i).depthmap = [];            
            map_horizon(i).cdata(:,:,1) = uint8(255*horizon_map);   map_horizon(i).cdata(:,:,2) = uint8(255*horizon_map);   map_horizon(i).cdata(:,:,3) = uint8(255*horizon_map);   map_horizon(i).depthmap = [];            
            
            disp(strcat(fileNames{f} , ': Percent Completed:', num2str(i/nof*100) ));
        end
        mov2yuv(intensity_map_name, map_brightness, format);
        mov2yuv(color_map_name1, map_color_1, format);
        mov2yuv(color_map_name2, map_color_2, format);
        mov2yuv(color_map_name3, map_color_3, format);
        mov2yuv(color_map_name4, map_color_4, format);
        mov2yuv(color_map_name5, map_color_5, format);
        mov2yuv(color_map_name6, map_color_6, format);
        mov2yuv(color_map_name7, map_color_7, format);
        mov2yuv(texture_map_name, map_texture, format);
        mov2yuv(motion_map_name1, map_motion_1, format);
        mov2yuv(motion_map_name2, map_motion_2, format);
        mov2yuv(motion_map_name3, map_motion_3, format);
        mov2yuv(motion_map_name4, map_motion_4, format);
        mov2yuv(motion_map_name5, map_motion_5, format);
        mov2yuv(motion_map_name6, map_motion_6, format);
        mov2yuv(motion_map_name7, map_motion_7, format);
        mov2yuv(depth_feature_map_name1, map_depth_1, format);
        mov2yuv(discomfort_map_name, map_discomfort, format);
        mov2yuv(person_map_name, map_person, format);
        mov2yuv(vehicle_map_name, map_vehicle, format);
        mov2yuv(animal_map_name, map_animal, format);
        mov2yuv(face_map_name, map_face, format);
        mov2yuv(text_map_name, map_text, format);
        mov2yuv(horizon_map_name, map_horizon, format);
        
     
% 		clear('map_color_1');	clear('map_color_2');	clear('map_color_3');	clear('map_color_4');	clear('map_color_5');	clear('map_color_6');	clear('map_color_7');	
%         clear('map_motion_1');	clear('map_motion_2');	clear('map_motion_3');	clear('map_motion_4');	clear('map_motion_5');	clear('map_motion_6');	clear('map_motion_7');	
%         clear('map_depth_1');	clear('map_discomfort_1');	clear('map_texture');	 clear('map_brightness');	
% 		clear('map_person');	clear('map_vehicle');	clear('map_animal');	clear('map_face');	clear('map_text');	clear('map_horizon');	
        
    end
end


% delete(gcp);
toc
