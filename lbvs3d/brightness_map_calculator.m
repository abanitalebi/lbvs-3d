function map = brightness_map_calculator(frame_yuv, labels, fovea_radius, fovea_mask)


    regsize = double(max(max(labels)));

    % Finding a variance map
    NHOOD = fspecial('disk',fovea_radius)>0; 
    y = frame_yuv(:,:,1);
    J = stdfilt(y, NHOOD);
    variance_map = J.^2;
    variance_map = variance_map / max(max(variance_map));

    % Centre-surround operator to find a brightness saliency map
    conspicuty_map = centre_surround(variance_map, labels, fovea_radius, regsize, fovea_mask);
    conspicuty_map = conspicuty_map / (max(max(conspicuty_map)));

    % Emphasize on local maxima
    conspicuty_map_maxima = emphasize_on_local_maxima(conspicuty_map);

    % Compactness & size effect: http://en.wikipedia.org/wiki/Image_moments
    compactness_map = compactness_map_generator(conspicuty_map_maxima, regsize, labels);
    
    map = conspicuty_map_maxima .* compactness_map;

end