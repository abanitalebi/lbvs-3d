function diff_map = absdiff_calculator(feature_map, feature_map_padded, fovea_radius, fovea_mask)

    [m,n] = size(feature_map);
    Dij = zeros(m,n);
    L = fovea_radius;
    feature_map_padded = padarray(feature_map_padded, [L+1,L+1], 'replicate', 'post');
    [mp,np] = size(feature_map_padded);
    
%     NHOOD = fspecial('disk',fovea_radius)>0;
      % Generating Foveation effect
%     mask_size = sum(sum(NHOOD));
    

    % Centre-Surround: difference calculator / fovea effect
%     for i = 1:mp-2*L
%         for j = 1:np-2*L
%             feature_block = feature_map_padded(i:i+2*L, j:j+2*L);       % A block around each element
%             x = feature_block ((L+1) , (L+1));
%             Dij(i,j) = sum(sum(abs(ones(2*L+1,2*L+1)*x - feature_block) .* fovea_mask ));
%         end
%     end
%     diff_map = Dij;

    for i = 1:mp-2*L
        for j = 1:np-2*L
            feature_block = feature_map_padded(i:i+2*L, j:j+2*L);       % A block around each element
            Dij(i,j) = sum(sum(abs(feature_block ((L+1) , (L+1)) - feature_block) .* fovea_mask ));
        end
    end
    diff_map = Dij;

end
    