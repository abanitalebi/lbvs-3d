function motion_maps = motion_map_calculator(ref_frame_R, current_frame_R, next_frame_R, ref_disparity_map, current_disparity_map, next_disparity_map, last_frame_flag, labels, display_width_cm, FR)
% Ref - Current - Next frame
% Dx, Dy, and Dz are in terms of cm, and represent the motion vector for the reference frame
% Positive and negative motion values are salient. Zero values are not salient.
% Dx > 0 means motion towards right; Dx < 0 means motion towrds left (?)
% Dy > 0 means motion towards down; Dy < 0 means motion towards up
% Dz > 0 means motion towards depth; Dz < 0 means motion towards the observer

    segsize = double(max(max(labels)));
    [height, width] = size(labels);
    
    % Finding the 3D motion vector: [Dx, Dy, Dz]
    st = load_settings('correlation_basic');
    show_flow = 0; % 1 = display the evolution of the flow, 0 = do not show
    h = '';
    ref_motion_vectors = motion_vector_generator (ref_frame_R, current_frame_R, st, show_flow, h, last_frame_flag, ref_disparity_map, current_disparity_map, labels, display_width_cm);
    ref_Dx = ref_motion_vectors(:,:,1);     ref_Dy = ref_motion_vectors(:,:,2);     ref_Dz = ref_motion_vectors(:,:,3);     
    current_motion_vectors = motion_vector_generator (current_frame_R, next_frame_R, st, show_flow, h, last_frame_flag, current_disparity_map, next_disparity_map, labels, display_width_cm);
    current_Dx = current_motion_vectors(:,:,1);     current_Dy = current_motion_vectors(:,:,2);     current_Dz = current_motion_vectors(:,:,3);     
    
    % Finding the motion features
    % Velocity magnitude
    ref_vx = (FR-1)*ref_Dx;       ref_vy = (FR-1)*ref_Dy;       ref_vz = (FR-1)*ref_Dz;       
    ref_v = sqrt(ref_vx.^2 + ref_vy.^2 + ref_vz.^2);
    
    % Emphasize on Dz
    ref_vz_emphasized = (FR-1)*(exp(ref_Dz) - 1);
    ref_v_Zemphasized = sqrt(ref_vx.^2 + ref_vy.^2 + ref_vz_emphasized.^2);
    
    % Acceleration
    current_vx = (FR-1)*current_Dx;       current_vy = (FR-1)*current_Dy;       current_vz = (FR-1)*current_Dz;       
    current_v = sqrt(current_vx.^2 + current_vy.^2 + current_vz.^2);
    ref_a = (FR-1)*(current_v - ref_v);
    
    % Element of Surprize
    hist3D = motion_histogram_3D (ref_Dx, ref_Dy, ref_Dz);    
    hist_map = exp(-hist3D/mean(mean(hist3D)));
    ref_surprize_element = zeros(height, width);  
    for k = 1:segsize
        segment_mask = double(labels==k);
        ref_surprize_element = (sum(sum(segment_mask .* hist_map)) / sum(sum(segment_mask))) * segment_mask + ref_surprize_element;
    end
    
    % Linear scaling to 0-1
    ref_Dx = (ref_Dx - min(min(ref_Dx)))/(max(max(ref_Dx))-min(min(ref_Dx)));
    ref_Dy = (ref_Dy - min(min(ref_Dy)))/(max(max(ref_Dy))-min(min(ref_Dy)));
    ref_Dz = (ref_Dz - min(min(ref_Dz)))/(max(max(ref_Dz))-min(min(ref_Dz)));
    ref_v = (ref_v - min(min(ref_v)))/(max(max(ref_v))-min(min(ref_v)));
    ref_v_Zemphasized = (ref_v_Zemphasized - min(min(ref_v_Zemphasized)))/(max(max(ref_v_Zemphasized))-min(min(ref_v_Zemphasized)));
    ref_a = (ref_a - min(min(ref_a)))/(max(max(ref_a))-min(min(ref_a)));
    ref_surprize_element = (ref_surprize_element - min(min(ref_surprize_element)))/(max(max(ref_surprize_element))-min(min(ref_surprize_element)));
    
    % Emphasize on local maxima
    ref_Dx_maxima = emphasize_on_local_maxima(ref_Dx);
    ref_Dy_maxima = emphasize_on_local_maxima(ref_Dy);
    ref_Dz_maxima = emphasize_on_local_maxima(ref_Dz);
    ref_v_maxima = emphasize_on_local_maxima(ref_v);
    ref_v_Zemphasized_maxima = emphasize_on_local_maxima(ref_v_Zemphasized);
    ref_a_maxima = emphasize_on_local_maxima(ref_a);
    ref_surprize_element_maxima = emphasize_on_local_maxima(ref_surprize_element);
    
    % Compactness and size effects
    ref_Dx_compactness = compactness_map_generator(ref_Dx_maxima, segsize, labels);                            ref_Dx_maxima_compact = ref_Dx_maxima .* ref_Dx_compactness;
    ref_Dy_compactness = compactness_map_generator(ref_Dy_maxima, segsize, labels);                            ref_Dy_maxima_compact = ref_Dy_maxima .* ref_Dy_compactness;
    ref_Dz_compactness = compactness_map_generator(ref_Dz_maxima, segsize, labels);                            ref_Dz_maxima_compact = ref_Dz_maxima .* ref_Dz_compactness;
    ref_v_compactness = compactness_map_generator(ref_v_maxima, segsize, labels);                            ref_v_maxima_compact = ref_v_maxima .* ref_v_compactness;
    ref_v_Zemphasized_compactness = compactness_map_generator(ref_v_Zemphasized_maxima, segsize, labels);       ref_v_Zemphasized_maxima_compact = ref_v_Zemphasized_maxima .* ref_v_Zemphasized_compactness;
    ref_a_compactness = compactness_map_generator(ref_a_maxima, segsize, labels);                            ref_a_maxima_compact = ref_a_maxima .* ref_a_compactness;
    ref_surprize_element_compactness = compactness_map_generator(ref_surprize_element_maxima, segsize, labels);     ref_surprize_element_maxima_compact = ref_surprize_element_maxima .* ref_surprize_element_compactness;
    
    motion_maps(:,:,1) = ref_Dx_maxima_compact;
    motion_maps(:,:,2) = ref_Dy_maxima_compact;
    motion_maps(:,:,3) = ref_Dz_maxima_compact;
    motion_maps(:,:,4) = ref_v_maxima_compact;
    motion_maps(:,:,5) = ref_v_Zemphasized_maxima_compact;
    motion_maps(:,:,6) = ref_a_maxima_compact;
    motion_maps(:,:,7) = ref_surprize_element_maxima_compact;

end