function hist3D = motion_histogram_3D (Dx, Dy, Dz)
    
    bin = 1000;

    [height, width] = size(Dx);
    hist3D = zeros(height, width);
         
    x = double(reshape(Dx, [1, height*width]));
    y = double(reshape(Dy, [1, height*width]));
    z = double(reshape(Dz, [1, height*width]));

    px = hist(x, bin);
    py = hist(y, bin);
    pz = hist(z, bin);
    
    minx = min(x);
    miny = min(y);
    minz = min(z);
    
    step_x = (max(x)-min(x))/bin;
    step_y = (max(y)-min(y))/bin;
    step_z = (max(z)-min(z))/bin;
    
    for i=1:height
        for j=1:width
            nx1 = floor((Dx(i,j)-minx)/step_x);
            ny1 = floor((Dy(i,j)-miny)/step_y);
            nz1 = floor((Dz(i,j)-minz)/step_z);
            
            if nx1 == 0
                nx1 = 1;
            end
            if ny1 == 0
                ny1 = 1;
            end
            if nz1 == 0
                nz1 = 1;
            end
            
            hist3D(i,j) = px(nx1)*py(ny1)*pz(nz1);
        end
    end
    
    hist3D = hist3D / sum(sum(sum(hist3D)));

end