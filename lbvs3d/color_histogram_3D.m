function hist3D = color_histogram_3D (frame_rgb)
    
    [height, width, dim] = size(frame_rgb);
    hist3D = zeros(height, width);
         
    r = double(reshape(frame_rgb(:,:,1), [1, height*width]));
    g = double(reshape(frame_rgb(:,:,2), [1, height*width]));
    b = double(reshape(frame_rgb(:,:,3), [1, height*width]));

    pr = hist(r, 256);
    pg = hist(g, 256);
    pb = hist(b, 256);
       
    for i=1:height
        for j=1:width
            r1 = frame_rgb(i,j,1)+1;
            g1 = frame_rgb(i,j,2)+1;
            b1 = frame_rgb(i,j,3)+1;
            hist3D(i,j) = pr(r1)*pg(g1)*pb(b1);
        end
    end
    
    hist3D = hist3D / sum(sum(sum(hist3D)));

end