% Ground-Truth and Saliency_Map are images in GrayScale format
tic
% This is to calculate the saliency metrics for test and ground_truth video folders

clear all; close all; clc;
warning off all

addpath('FastEMD-3.1');
addpath('shuffled-AUC');

% Create a parallel MATLAB Pool (version 2014a)
% delete(gcp);
% parpool(6);

% % Create a parallel MATLAB pool (version 2012b)
% matlabpool('open',4);
% matlabpool('close');      % This goes at the end of the program

% It is assumed that the files in the Test and GroundTruth folders are in a similar order
groundtruth_folder_path = 'H:\PhD\Documents\3DSaliency\3AminsWork\Codes\Drafts\Saliency_Amin\FixationMaps\Test\Ground_Truth';
saliency_folder_path = 'H:\PhD\Documents\3DSaliency\3AminsWork\Codes\Drafts\Saliency_Amin\FixationMaps\Test\Amin';

Average_Fixation_Map_3D = double(imread('Average_Fixation_Map_3D.png'));


nof_max = 1000;                   % number of frames for which you would like to generate the saliency map. min(nof_max, total frames in the file) will be used.
width = 1920;
height = 1080;
format = '420';

resize_factor = 8;      % resize_factor = 1 will use the original video resolution. It makes the code slow. / default=8
dims = round([height, width]/resize_factor); 
Nsize = dims(1)*dims(2)+2;

dirOutput_GTruth = dir(fullfile(groundtruth_folder_path,'*.yuv'));  fileNames_Gtruth = {dirOutput_GTruth.name}';  numFiles = numel(fileNames_Gtruth);
dirOutput_saliency = dir(fullfile(saliency_folder_path,'*.yuv'));        fileNames_saliency = {dirOutput_saliency.name}';  numSaliencyMaps = numel(fileNames_saliency);
h=fspecial('gaussian', 50, 10);
h=h/max(h(:));

for f=1:numFiles
% for f=1:21
        
    Gtruth_map_name = strcat(groundtruth_folder_path,'\',fileNames_Gtruth{f});
    Saliency_map_name = strcat(saliency_folder_path,'\',fileNames_saliency{f});
    
    num_frames_yuv = yuv_frame_number(Gtruth_map_name,width,height,format);
    nof = min(num_frames_yuv, nof_max);
    
    emd = 0;    s_measure = 0;      areaUnderROC = 0;   RHO = 0;    KL = 0; NSS=0;  AUC_shuffled=0;   FPRs = zeros(Nsize,1);    TPRs = zeros(Nsize,1);  FPs = zeros(Nsize,1);   TPs = zeros(Nsize,1);
    for i = 1:nof
    
        Gtruth_map = rgb2gray(catch_frame_from_YUV_file (Gtruth_map_name , i , width, height, format));
        Gtruth_map_dims = imresize(Gtruth_map, dims);
        Saliency_map0 = rgb2gray(catch_frame_from_YUV_file (Saliency_map_name , i , width, height, format));
        
%         % only to test the performance of the demo files: add blurring and clip the blurred video
%         x = double(Saliency_map00) / double(max(Saliency_map00(:)));
%         y = imfilter(x,h);
%         y = y / max(y(:));
%         y(y<0.1) = 0;
%         Saliency_map0 = y;
        
%         % add blur & center bias to test the AUC: blur: 5,10,15,20,30,40,50 & CenterBias: 20,30,50,70,100,150,200,250,300,400,500
%         CenterBias = 100;     blur_factor = 15;
%         h = fspecial('gaussian', [height, width], CenterBias);      h = h / max(h(:));      Saliency_map0 = double(Saliency_map0) / double(max(Saliency_map0(:)));
%         hh = fspecial('gaussian', [height, width], blur_factor);      hh = hh / max(hh(:));
%         Saliency_map1 = (Saliency_map0 + h) / 2;
%         Saliency_map = imfilter(Saliency_map1, hh);

        Saliency_map = Saliency_map0;
        
        Saliency_map_dims = imresize(Saliency_map, dims);
        shufMap_dims = imresize(Average_Fixation_Map_3D, dims); 
            
        [emd_frame, s_measure_frame, areaUnderROC_frame, FPRs_frame, TPRs_frame, FPs_frame, TPs_frame, RHO_frame, KL_frame, NSS_frame, AUC_shuffled_frame] = saliency_metrics_per_frame(Gtruth_map_dims, Saliency_map_dims, shufMap_dims);
        
        emd = emd + emd_frame;
        s_measure = s_measure + s_measure_frame;
        areaUnderROC = areaUnderROC + areaUnderROC_frame;
        KL = KL + KL_frame;
        NSS = NSS + NSS_frame;
        AUC_shuffled = AUC_shuffled + AUC_shuffled_frame;
        RHO = RHO + RHO_frame;
        
        FPRs_frame1 = padarray(FPRs_frame, Nsize-length(FPRs_frame), 'symmetric', 'post');
        TPRs_frame1 = padarray(TPRs_frame, Nsize-length(TPRs_frame), 'symmetric', 'post');
        FPs_frame1 = padarray(FPs_frame, Nsize-length(FPs_frame), 'symmetric', 'post');
        TPs_frame1 = padarray(TPs_frame, Nsize-length(TPs_frame), 'symmetric', 'post');
        
        FPRs = FPRs + FPRs_frame1;
        TPRs = TPRs + TPRs_frame1;
        FPs = FPs + FPs_frame1;
        TPs = TPs + TPs_frame1;
        
    end

    emd_video(f) = emd / nof;
    s_measure_video(f) = s_measure / nof;
    areaUnderROC_video(f) = areaUnderROC / nof;
    FPRs_video(f,1:Nsize) = FPRs / nof;
    TPRs_video(f,1:Nsize) = TPRs / nof;
    FPs_video(f,1:Nsize) = FPs / nof;
    TPs_video(f,1:Nsize) = TPs / nof;
    RHO_video(f) = RHO / nof;
    KL_video(f) = KL / nof;
    NSS_video(f) = NSS / nof;
    AUC_shuffled_video(f) = AUC_shuffled / nof;
     
    clc; disp('percent completed:  '); disp(f/numFiles*100);
end

% save(strcat( 'saliency_evaluation_metrics_Amin_nTrees_40_nof_max_Train_20_numFeatures_24_resize_factor8_blur_', num2str(blur_factor), '_CenterBias_', num2str(CenterBias), '_AUC_', num2str(mean(areaUnderROC_video)), '_', num2str(mean(AUC_shuffled_video)), '.mat'), 'emd_video', 's_measure_video', 'areaUnderROC_video', 'FPRs_video', 'TPRs_video', 'FPs_video', 'TPs_video', 'RHO_video', 'KL_video', 'NSS_video', 'AUC_shuffled_video');
save('saliency_evaluation_metrics_Amin_test.mat', 'emd_video', 's_measure_video', 'areaUnderROC_video', 'FPRs_video', 'TPRs_video', 'FPs_video', 'TPs_video', 'RHO_video', 'KL_video', 'NSS_video', 'AUC_shuffled_video');
mean(areaUnderROC_video)
mean(AUC_shuffled_video)
mean(emd_video)
mean(s_measure_video)
mean(RHO_video)
mean(KL_video)
mean(NSS_video)

saliency_folder_path

% ROC curve: 
% plot([sort(mean(FPRs_video,1)), 1], [sort(mean(TPRs_video,1)),1]);
% ROC curve for one specific video: plot([sort(FPRs_video(1,:)),1], [sort(TPRs_video(1,:)),1]);

