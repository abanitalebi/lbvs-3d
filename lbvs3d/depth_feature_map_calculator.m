function depth_features = depth_feature_map_calculator(depth_map, labels, resize_factor)

    segsize = max(max(labels));
    [height, width] = size(labels);
    
    % Findingthe depth map when only one depth value is assigned to each segment
    depth_segmented = zeros(height, width);  
    for k = 1:segsize
        segment_mask = double(labels==k);
        depth_segmented = (sum(sum(segment_mask .* depth_map)) / sum(sum(segment_mask))) * segment_mask + depth_segmented;
    end

    % Depth abruptness
    depth_abruptness = depth_abruptness_generation (labels, depth_segmented, segsize, height, width, resize_factor);

    % Emphasize on local maxima
    depth_abruptness_maxima = emphasize_on_local_maxima(depth_abruptness);
    
    % Effect of size and compactness
    compactness_map = compactness_map_generator(depth_abruptness_maxima, segsize, labels);    
    
    depth_features = depth_abruptness_maxima .* compactness_map;
end