function Gmap = gabor_energy2D(im_Y)
% an example to demonstrate the use of gabor filter. 
% the results mimic: http://matlabserver.cs.rug.nl/edgedetectionweb/web/edgedetection_examples.html
% Wavelength = 8 ; Orientation(s) (deg.) = 0 ; Phase offset(s) (deg.) = 0, 90 ; Aspect Ratio = 0.5 ; Bandwidth = 1 ; Number of orientations = 8 
% Gabor Filter Energy

    lambda  = 8;
    theta   = 0;
    psi     = [0 pi/2];
    gamma   = 0.5;
    bw      = 1;
    N       = 8;

    img_in = im2double(im_Y);

    img_out = zeros(size(img_in,1), size(img_in,2), N);
    for n=1:N
        gb = gabor_fn(bw,gamma,psi(1),lambda,theta)...
            + 1i * gabor_fn(bw,gamma,psi(2),lambda,theta);
        % gb is the n-th gabor filter
        img_out(:,:,n) = imfilter(img_in, gb, 'symmetric');
        % filter output to the n-th channel
        theta = theta + 2*pi/N;
        % next orientation
    end
    img_out_disp = sum(abs(img_out).^2, 3).^0.5;
    img_out_disp = img_out_disp./max(img_out_disp(:));
    
    Gmap = img_out_disp;

end