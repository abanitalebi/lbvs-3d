function conspicuty_map_maxima = emphasize_on_local_maxima(conspicuty_map)
    m = mean(mean(conspicuty_map));
    temp2 = (exp(conspicuty_map)-m);
    mi = min(min(temp2));  ma = max(max(temp2));
    conspicuty_map_maxima = (temp2-mi)/(ma-mi);
end
