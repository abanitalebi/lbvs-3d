tic
% This is to fuse the conspicuty maps to generate a saliency map using Random Forests

clear all; close all; clc;
warning off all

nTrees = 40;
nof_max_Train = 20;     % number of frames used to train the random forest              
numFeatures = 24;
resize_factor = 8;      % resize_factor = 1 will use the original video resolution. It makes the code slow. / default=8
nof_max_Test = 1000;      % number of frames for which you would like to generate the saliency map. min(nof_max_Test, total frames in the file) will be used.

% Random frame selection, randperm

% Create a parallel MATLAB Pool (version 2014a)
% delete(gcp);
% parpool(6);

% % Create a parallel MATLAB pool (version 2012b)
% matlabpool('open',4);
% matlabpool('close');      % This goes at the end of the program

Train_conspicuty_map_folder_path = 'H:\PhD\Documents\3DSaliency\3AminsWork\Codes\Drafts\Saliency_Amin\GeneratedMaps\Train';
Train_fixation_map_folder_path = 'H:\PhD\Documents\3DSaliency\3AminsWork\Codes\Drafts\Saliency_Amin\FixationMaps\Train\Ground_Truth';
Test_conspicuty_map_folder_path = 'H:\PhD\Documents\3DSaliency\3AminsWork\Codes\Drafts\Saliency_Amin\GeneratedMaps\Test';
Test_fixation_map_folder_path = 'H:\PhD\Documents\3DSaliency\3AminsWork\Codes\Drafts\Saliency_Amin\FixationMaps\Test';

MinLeaf = 10;   FBoot = 1;  SampleWithReplacement = 'True';     SampleRatio = 1/3;
% nTrees=255; nof_max_Train = 50; resize_factor = 4; 'more videos'; MinLeaf = 10; FBoot=1; SampleWithReplacement = 'True';   SampleRatio = 1/3;   

rng default
type = 'regression';
% type = 'classification';


width = 1920;
height = 1080;
format = '420';
labels = {'animal', 'brightness', 'color1', 'color2', 'color3', 'color4', 'color5', 'color6', 'color7', 'depth', 'discomfort', 'face', 'horizon', 'motion1', 'motion2', 'motion3', 'motion4', 'motion5', 'motion6', 'motion7', 'person', 'text', 'texure', 'vehicle'};
dims = round([height, width]/resize_factor); 

dirOutput_train = dir(fullfile(Train_conspicuty_map_folder_path,'*.yuv'));  fileNames_train = {dirOutput_train.name}';  numFiles = numel(fileNames_train);
dirOutput_fixations = dir(fullfile(Train_fixation_map_folder_path,'*.yuv'));        fileNames_fixations = {dirOutput_fixations.name}';  numFixMaps = numel(fileNames_fixations);

dirOutput_test = dir(fullfile(Test_conspicuty_map_folder_path,'*.yuv'));  fileNames_Test = {dirOutput_test.name}';  numFiles_test = numel(fileNames_Test);
dirOutput_fixations_test = dir(fullfile(Test_fixation_map_folder_path,'*.yuv'));        fileNames_fixations_test = {dirOutput_fixations_test.name}';  numFixMaps_test = numel(fileNames_fixations_test);


% Training
count_fix_maps = 0;     frame_count = 0;
% conspicuty_maps = zeros(dims(1), dims(2), numFeatures, numFixMaps);
% fixation_maps = zeros(dims(1), dims(2), numFixMaps);
% for f=1:numFeatures:numFiles
%         
%     conspicuty_map_1 = strcat(Train_conspicuty_map_folder_path,'\',fileNames_train{f});
%     conspicuty_map_2 = strcat(Train_conspicuty_map_folder_path,'\',fileNames_train{f+1});
%     conspicuty_map_3 = strcat(Train_conspicuty_map_folder_path,'\',fileNames_train{f+2});
%     conspicuty_map_4 = strcat(Train_conspicuty_map_folder_path,'\',fileNames_train{f+3});
%     conspicuty_map_5 = strcat(Train_conspicuty_map_folder_path,'\',fileNames_train{f+4});
%     conspicuty_map_6 = strcat(Train_conspicuty_map_folder_path,'\',fileNames_train{f+5});
%     conspicuty_map_7 = strcat(Train_conspicuty_map_folder_path,'\',fileNames_train{f+6});
%     conspicuty_map_8 = strcat(Train_conspicuty_map_folder_path,'\',fileNames_train{f+7});
%     conspicuty_map_9 = strcat(Train_conspicuty_map_folder_path,'\',fileNames_train{f+8});
%     conspicuty_map_10 = strcat(Train_conspicuty_map_folder_path,'\',fileNames_train{f+9});
%     conspicuty_map_11 = strcat(Train_conspicuty_map_folder_path,'\',fileNames_train{f+10});
%     conspicuty_map_12 = strcat(Train_conspicuty_map_folder_path,'\',fileNames_train{f+11});
%     conspicuty_map_13 = strcat(Train_conspicuty_map_folder_path,'\',fileNames_train{f+12});
%     conspicuty_map_14 = strcat(Train_conspicuty_map_folder_path,'\',fileNames_train{f+13});
%     conspicuty_map_15 = strcat(Train_conspicuty_map_folder_path,'\',fileNames_train{f+14});
%     conspicuty_map_16 = strcat(Train_conspicuty_map_folder_path,'\',fileNames_train{f+15});
%     conspicuty_map_17 = strcat(Train_conspicuty_map_folder_path,'\',fileNames_train{f+16});
%     conspicuty_map_18 = strcat(Train_conspicuty_map_folder_path,'\',fileNames_train{f+17});
%     conspicuty_map_19 = strcat(Train_conspicuty_map_folder_path,'\',fileNames_train{f+18});
%     conspicuty_map_20 = strcat(Train_conspicuty_map_folder_path,'\',fileNames_train{f+19});
%     conspicuty_map_21 = strcat(Train_conspicuty_map_folder_path,'\',fileNames_train{f+20});
%     conspicuty_map_22 = strcat(Train_conspicuty_map_folder_path,'\',fileNames_train{f+21});
%     conspicuty_map_23 = strcat(Train_conspicuty_map_folder_path,'\',fileNames_train{f+22});
%     conspicuty_map_24 = strcat(Train_conspicuty_map_folder_path,'\',fileNames_train{f+23});
%     
%     count_fix_maps = count_fix_maps + 1;
%     fixation_map_name = strcat(Train_fixation_map_folder_path,'\',fileNames_fixations{count_fix_maps});
%     
%     num_frames_yuv = yuv_frame_number(conspicuty_map_1,width,height,format);
%     nof = min(num_frames_yuv, nof_max_Train);
%     
% %     perm = randperm(num_frames_yuv);
% %     train_frames = perm(1:nof);
%     
% %     for iii = 1:length(train_frames)
% %         i = train_frames(iii);
%      for i = 1:nof
%               
%         % Conspicuty maps
%         frame_count = frame_count + 1;
%         for jj = 1:numFeatures
%             conspicuty_map = rgb2gray(catch_frame_from_YUV_file (eval(strcat('conspicuty_map_', num2str(jj))) , i , width, height, format));
%             conspicuty_map_dims = imresize(conspicuty_map, dims);
%             conspicuty_maps (:,:, frame_count, jj) = double(conspicuty_map_dims) / double(max(conspicuty_map_dims(:)));
%         end
%         
%         % Fixation map
%         fixation_map_orig = rgb2gray(catch_frame_from_YUV_file (fixation_map_name , i , width, height, format));
%         fixation_map_uint = imresize(fixation_map_orig, dims);
%         fixation_maps (:,:,frame_count) = double(fixation_map_uint) / double(max(fixation_map_uint(:)));
% %         fixation_maps(:,:,frame_count) = fixation_map_uint;
%          
% %         disp(strcat(fileNames_fixations{count_fix_maps} , ': Percent Completed:', num2str(iii/length(train_frames)*100) ));
%         disp(strcat(fileNames_fixations{count_fix_maps} , ': Percent Completed:', num2str(i/nof*100) ));
%     end
%         
% end
% conspicuty_maps(isnan(conspicuty_maps)) = 0;
% fixation_maps(isnan(fixation_maps)) = 0;

% Training a model
% [h, w, nF, nFeatures] = size(conspicuty_maps);
% NVarToSample = floor(h*w*nF*SampleRatio);
RFmodel_name = strcat( 'RFmodel', '_nTrees_', num2str(nTrees), '_nof_max_Train_', num2str(nof_max_Train), '_numFeatures_', num2str(numFeatures), '_resize_factor', num2str(resize_factor), '.mat' );
load(RFmodel_name);
% [RFmodel, FeatureImportance] = train_saliency_RF(conspicuty_maps, fixation_maps, nTrees, type, MinLeaf, FBoot);
FeatureImportance = FeatureImportance / max(FeatureImportance(:));
[Importances, locations] = sort(FeatureImportance, 'descend');
ImportantFeatures = labels(locations);

% clear('conspicuty_maps');
% clear('fixation_maps');
% save(RFmodel_name, '-v7.3', 'RFmodel', 'FeatureImportance');
clc
disp('Training finished, now testing ...');        

% Testing the created model over the test videos
count_fix_maps = 0;     frame_count = 0;
for f=1:numFeatures:numFiles_test
        
    conspicuty_map_1 = strcat(Test_conspicuty_map_folder_path,'\',fileNames_Test{f});
    conspicuty_map_2 = strcat(Test_conspicuty_map_folder_path,'\',fileNames_Test{f+1});
    conspicuty_map_3 = strcat(Test_conspicuty_map_folder_path,'\',fileNames_Test{f+2});
    conspicuty_map_4 = strcat(Test_conspicuty_map_folder_path,'\',fileNames_Test{f+3});
    conspicuty_map_5 = strcat(Test_conspicuty_map_folder_path,'\',fileNames_Test{f+4});
    conspicuty_map_6 = strcat(Test_conspicuty_map_folder_path,'\',fileNames_Test{f+5});
    conspicuty_map_7 = strcat(Test_conspicuty_map_folder_path,'\',fileNames_Test{f+6});
    conspicuty_map_8 = strcat(Test_conspicuty_map_folder_path,'\',fileNames_Test{f+7});
    conspicuty_map_9 = strcat(Test_conspicuty_map_folder_path,'\',fileNames_Test{f+8});
    conspicuty_map_10 = strcat(Test_conspicuty_map_folder_path,'\',fileNames_Test{f+9});
    conspicuty_map_11 = strcat(Test_conspicuty_map_folder_path,'\',fileNames_Test{f+10});
    conspicuty_map_12 = strcat(Test_conspicuty_map_folder_path,'\',fileNames_Test{f+11});
    conspicuty_map_13 = strcat(Test_conspicuty_map_folder_path,'\',fileNames_Test{f+12});
    conspicuty_map_14 = strcat(Test_conspicuty_map_folder_path,'\',fileNames_Test{f+13});
    conspicuty_map_15 = strcat(Test_conspicuty_map_folder_path,'\',fileNames_Test{f+14});
    conspicuty_map_16 = strcat(Test_conspicuty_map_folder_path,'\',fileNames_Test{f+15});
    conspicuty_map_17 = strcat(Test_conspicuty_map_folder_path,'\',fileNames_Test{f+16});
    conspicuty_map_18 = strcat(Test_conspicuty_map_folder_path,'\',fileNames_Test{f+17});
    conspicuty_map_19 = strcat(Test_conspicuty_map_folder_path,'\',fileNames_Test{f+18});
    conspicuty_map_20 = strcat(Test_conspicuty_map_folder_path,'\',fileNames_Test{f+19});
    conspicuty_map_21 = strcat(Test_conspicuty_map_folder_path,'\',fileNames_Test{f+20});
    conspicuty_map_22 = strcat(Test_conspicuty_map_folder_path,'\',fileNames_Test{f+21});
    conspicuty_map_23 = strcat(Test_conspicuty_map_folder_path,'\',fileNames_Test{f+22});
    conspicuty_map_24 = strcat(Test_conspicuty_map_folder_path,'\',fileNames_Test{f+23});
    
    count_fix_maps = count_fix_maps + 1;
    saliency_map_name = strcat(Test_fixation_map_folder_path,'\',fileNames_Test{f});        saliency_map_name = strcat(  saliency_map_name(1:end-11), '.yuv'  );
    saliency_map_name = strcat( saliency_map_name(1:end-3), '_nTrees_', num2str(nTrees), '_nof_max_Train_', num2str(nof_max_Train), '_numFeatures_', num2str(numFeatures), '_resize_factor', num2str(resize_factor), '.yuv' );

    num_frames_yuv = yuv_frame_number(conspicuty_map_1,width,height,format);
    nof = min(num_frames_yuv, nof_max_Test);

    for i = 1:nof
        
        % Conspicuty maps
        for jj = 1:numFeatures
            conspicuty_map = rgb2gray(catch_frame_from_YUV_file (eval(strcat('conspicuty_map_', num2str(jj))) , i , width, height, format));
            conspicuty_map_dims = imresize(conspicuty_map, dims);
            conspicuty_maps_test (:,:, jj) = double(conspicuty_map_dims) / double(max(conspicuty_map_dims(:)));
        end
        conspicuty_maps_test(isnan(conspicuty_maps_test)) = 0;
        test_features = reshape(conspicuty_maps_test, [dims(1)*dims(2), numFeatures]);

        % Testing a model
        classes = test_saliency_RF(RFmodel, test_features, type);
        saliency_map_0 = reshape(classes, dims);
        saliency_map_1 = imresize(saliency_map_0, [height, width]);
        Saliency_map2 = uint8(255*saliency_map_1 / max(saliency_map_1(:)));
        
        % add blur & center bias to test the AUC: blur: 5  &  CenterBias: 70
        CenterBias = 70;     blur_factor = 5;
        hm = fspecial('gaussian', [height, width], CenterBias);      hm = hm / max(hm(:));      Saliency_map2 = double(Saliency_map2) / double(max(Saliency_map2(:)));
        hhm = fspecial('gaussian', [height, width], blur_factor);      hhm = hhm / max(hhm(:));
        Saliency_map3 = (Saliency_map2 + hm) / 2;
        saliency_map_4 = imfilter(Saliency_map3, hhm);
        saliency_map_4 = uint8(255*saliency_map_4 / max(saliency_map_4(:)));
              
        
        saliency_maps(i).cdata(:,:,1) = saliency_map_4;     saliency_maps(i).cdata(:,:,2) = saliency_map_4;     saliency_maps(i).cdata(:,:,3) = saliency_map_4;      saliency_maps(i).colormap = [];

        disp(strcat(saliency_map_name , ': Percent Completed:', num2str(i/nof*100) ));
    end
    mov2yuv(saliency_map_name, saliency_maps, format);
    clear('saliency_maps');
    clear('conspicuty_maps');

end
% clear
toc
