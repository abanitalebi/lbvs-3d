function current_subject_frames = read_yuv_folder_frames(input_video_folder_path, nof, numFiles, height, width, resize_factor)
    
    
    dirOutput = dir(fullfile(input_video_folder_path,'*.yuv'));
    fileNames = {dirOutput.name}';
%     width = 1920;   height = 1080;      
    format = '420';
%     resize_factor = 8;
    dims(1) = floor(height/resize_factor);
    dims(2) = floor(width/resize_factor);
    current_subject_frames = zeros(dims(1), dims(2), nof, numFiles);
    for f=1:numFiles
        YUV_video_name = strcat(input_video_folder_path,'\',fileNames{f});
        for i = 1:nof  
            frame_rgb_0 = catch_frame_from_YUV_file(YUV_video_name, i , width, height, format);
            frame = imresize(rgb2gray(frame_rgb_0), dims);
            current_subject_frames(:,:,i,f) = double(frame) / double(max(frame(:)));
        end
    end
    
end