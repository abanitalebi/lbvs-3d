function maps = color_map_calculator(frame_Lab, frame_rgb, labels, fovea_radius, fovea_mask)
      
    segsize = double(max(max(labels)));
    [height, width] = size(labels);

    % 1) 3D color histogram: emphasize on segments with rare color
    hist3D = color_histogram_3D (frame_rgb);    
    hist_map = exp(-hist3D/mean(mean(hist3D)));
        
    % 2) Finding a variance map
    NHOOD = fspecial('disk',fovea_radius)>0; 
    L = frame_Lab(:,:,1);
    a = frame_Lab(:,:,2);
    b = frame_Lab(:,:,3);
    J1= stdfilt(a, NHOOD);
    J2= stdfilt(b, NHOOD);
    variance_map1 = J1.^2;
    variance_map2 = J2.^2;
    variance_map1 = variance_map1 / max(max(variance_map1));
    variance_map2 = variance_map2 / max(max(variance_map2));
    
    % 2) Centre-surround operator to find a brightness saliency map
    contrast_map1 = centre_surround(variance_map1, labels, fovea_radius, segsize, fovea_mask);
    contrast_map2 = centre_surround(variance_map2, labels, fovea_radius, segsize, fovea_mask);  
    contrast_map1 = contrast_map1 / (max(max(contrast_map1)));
    contrast_map2 = contrast_map2 / (max(max(contrast_map2)));

    % 3) Emphasize on warm colors
    
    xyz = rgb2xyz(frame_rgb);
    xy = xyz2xy(xyz);
    temperature = xy2cct(xy);
    for i0=1:height
        for j0=1:width
            if temperature(i0,j0)>15000
                temperature(i0,j0)=15000;
            end
            if temperature(i0,j0)<1500
                temperature(i0,j0)=1500;
            end
        end
    end
    for i=1:height
        for j=1:width
            if isnan(temperature(i,j))
                temperature(i,j) = 1500;
            end
        end
    end
    warmth_map = 1 ./ temperature;
    warmth_map = warmth_map / max(max(warmth_map));
    
    % 4) Emphasize on saturated colors
    Cab = sqrt(a.^2 + b.^2);
    saturation_map = Cab ./ sqrt(Cab.^2 + L.^2);
    for i=1:height
        for j=1:width
            if isnan(saturation_map(i,j))
                saturation_map(i,j) = 0;
            end
        end
    end
                
    
    % 5) HVS color sensitivity: CIE 1978 specifies a ratio of 0-1 for each wavelength
    hue = 360 * rgb2hsv_fast(frame_rgb,'','H');
    wavelength = 650 - hue/1.37143;
    sensitivitiy_map = zeros(height, width);
    for i0 = 1:height
        for j0=1:width
            sensitivity_map(i0,j0) = wavelength2sensitivity(wavelength(i0,j0));
        end
    end
    
    % 6) Probability of being a salient colro using fixation analysis on different colors
    prob_map = prob_using_color_fixations(frame_rgb);
    
    % Assigning one value per each segment for cases: 1, 3, 4, 5, and 6
    color_hist_map = zeros([height, width]);
    color_warmth_map = zeros([height, width]);
    color_saturation_map = zeros([height, width]);
    color_sensitivity_map = zeros([height, width]);
    color_prob_map = zeros([height, width]);
    
    for k = 1:segsize
        segment_mask = double(labels==k);
        color_hist_map = color_hist_map + ((sum(sum(segment_mask .* hist_map)) / (sum(sum(segment_mask)))) * segment_mask);
        color_warmth_map = color_warmth_map + ((sum(sum(segment_mask .* warmth_map)) / (sum(sum(segment_mask)))) * segment_mask);
        color_saturation_map = color_saturation_map + ((sum(sum(segment_mask .* saturation_map)) / (sum(sum(segment_mask)))) * segment_mask);
        color_sensitivity_map = color_sensitivity_map + ((sum(sum(segment_mask .* sensitivity_map)) / (sum(sum(segment_mask)))) * segment_mask);
        color_prob_map = color_prob_map + ((sum(sum(segment_mask .* prob_map)) / (sum(sum(segment_mask)))) * segment_mask);
    end
    
    % Emphasize on local maxima
    color_hist_map_maxima = emphasize_on_local_maxima(color_hist_map);
    color_warmth_map_maxima = emphasize_on_local_maxima(color_warmth_map);
    color_saturation_map_maxima = emphasize_on_local_maxima(color_saturation_map);
    color_sensitivity_map_maxima = emphasize_on_local_maxima(color_sensitivity_map);
    color_prob_map_maxima = emphasize_on_local_maxima(color_prob_map);
    color_contrast_map_maxima1 = emphasize_on_local_maxima(contrast_map1);
    color_contrast_map_maxima2 = emphasize_on_local_maxima(contrast_map2);
    
    % Compactness & size effect: http://en.wikipedia.org/wiki/Image_moments
    hist_compactness = compactness_map_generator(color_hist_map_maxima, segsize, labels);                            color_hist_map_maxima_compact = color_hist_map_maxima .* hist_compactness;
    warmth_compactness = compactness_map_generator(color_warmth_map_maxima, segsize, labels);                        color_warmth_map_maxima_compact = color_warmth_map_maxima .* warmth_compactness;
    saturation_compactness = compactness_map_generator(color_saturation_map_maxima, segsize, labels);                color_saturation_map_maxima_compact = color_saturation_map_maxima .* saturation_compactness;
    sensitivity_compactness = compactness_map_generator(color_sensitivity_map_maxima, segsize, labels);              color_sensitivity_map_maxima_compact = color_sensitivity_map_maxima .* sensitivity_compactness;
    prob_compactness = compactness_map_generator(color_prob_map_maxima, segsize, labels);                            color_prob_maxima_compact = color_prob_map_maxima .* prob_compactness;
    contrast1_compactness = compactness_map_generator(color_contrast_map_maxima1, segsize, labels);                  color_contrast_map_maxima1_compact = color_contrast_map_maxima1 .* contrast1_compactness;
    contrast2_compactness = compactness_map_generator(color_contrast_map_maxima2, segsize, labels);                  color_contrast_map_maxima2_compact = color_contrast_map_maxima2 .* contrast2_compactness;
    
    maps(:,:,1) = color_hist_map_maxima_compact;
    maps(:,:,2) = color_warmth_map_maxima_compact;
    maps(:,:,3) = color_saturation_map_maxima_compact;
    maps(:,:,4) = color_sensitivity_map_maxima_compact;
    maps(:,:,5) = color_prob_maxima_compact;
    maps(:,:,6) = color_contrast_map_maxima1_compact;
    maps(:,:,7) = color_contrast_map_maxima2_compact;
        
end