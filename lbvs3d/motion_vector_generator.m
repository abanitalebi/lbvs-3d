function motion_vectors = motion_vector_generator (ref_frame_R, current_frame_R, st, show_flow, h, last_frame_flag, ref_disparity_map, current_disparity_map, labels, display_width_cm)
    
    segsize = double(max(max(labels)));
    [height, width] = size(labels);
    % Use optical flow to find Dx & Dy
    [flow] = coarse_to_fine(ref_frame_R, current_frame_R, st, show_flow, h);      % Obtain motion vectors of ref_frame
    Dx = flow(:, :, 1);             % Horizontal motion: Dx = x2-x1. So if Dx > 0 it means that object has moved towards right direction: x2>x1
    Dy = flow(:, :, 2);             % Vertical motion: Dy = y2-y1. Positive Dy > 0 means y2>y1, therefore y2 is lower
    if last_frame_flag == 1
        Dx = -Dx;
        Dy = -Dy;
    end
    Dx = display_width_cm / width * Dx;
    Dy = display_width_cm / width * Dy;
    
    % Finding the Z-motion vector: Dz: Z is the direction of depth. The deeper an object is, the higher its Z would be. if Dz>0, then Z2>Z1 and this means that in the next frame the object would go deeper.
%     Diff_map_Z = ref_disparity_map - current_disparity_map;  
    Diff_map_Z = zeros(height, width);
    Dxx = round(Dx);    Dyy = round(Dy);
    ref_depth_map = disparity2depth(ref_disparity_map);
    current_depth_map = disparity2depth(current_disparity_map);
    for i = 1:height
        for j = 1:width
            current_y = i+Dyy(i,j);
            current_x = j+Dxx(i,j);
            if (j+Dxx(i,j) <= 1)
                current_x = 1;
            end
            if (j+Dxx(i,j) >= width)
                current_x = width;
            end
            if (i+Dyy(i,j) <= 1)
                current_y = 1;
            end
            if (i+Dyy(i,j) >= height)
                current_y = height;
            end     
            Diff_map_Z(i,j) = ref_depth_map(i,j) - current_depth_map(current_y , current_x);
        end
    end  
    Dx_segmented = zeros(height, width);        Dy_segmented = zeros(height, width);        Dz_segmented = zeros(height, width);
    for k = 1:segsize
        segment_mask = double(labels==k);
        Dx_segmented = (sum(sum(segment_mask .* Dx)) / sum(sum(segment_mask))) * segment_mask + Dx_segmented;
        Dy_segmented = (sum(sum(segment_mask .* Dy)) / sum(sum(segment_mask))) * segment_mask + Dy_segmented;
        Dz_segmented = (sum(sum(segment_mask .* Diff_map_Z)) / sum(sum(segment_mask))) * segment_mask + Dz_segmented;
    end
   motion_vectors(:,:,1) = Dx_segmented;
   motion_vectors(:,:,2) = Dy_segmented;
   motion_vectors(:,:,3) = Dz_segmented;
    
end