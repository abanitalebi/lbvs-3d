function [RFmodel, FeatureImportance] = train_saliency_RF(conspicuty_maps, fixation_maps, nTrees, type, MinLeaf, FBoot)
    
    % Two possible approaches: 1) classification using 256 classes 2) Regression
    % Pruning is on
    
    features4D = conspicuty_maps;     % 4-D matrix: [height, width, numFeatures(24), total number of frames in all training videos]
    classLabels3D  = fixation_maps;         % 3-D matrix: [height, width, total number of frames in all training videos]
    
    [height, width, numFrames, numFeatures] = size(features4D);
    features = reshape(features4D, [height*width*numFrames, numFeatures]);
    
    classLabels = double(reshape(classLabels3D, [height*width*numFrames, 1]));
    
    % How many trees do you want in the forest? 
    % nTrees = 20;
    
    % Train the TreeBagger (Decision Forest).
    if strcmp(type, 'classification')
        RFmodel = TreeBagger(nTrees,features,classLabels, 'Method', 'classification', 'MinLeaf', MinLeaf, 'FBoot', FBoot, 'OOBVarImp', 'on');
    end
    if strcmp(type, 'regression')
        RFmodel = TreeBagger(nTrees,features,classLabels, 'Method', 'R', 'MinLeaf', MinLeaf, 'FBoot', FBoot, 'OOBVarImp', 'on');
    end
    
    FeatureImportance = RFmodel.OOBPermutedVarDeltaError;         % The more increase in prediction Error ==> The more important the variable is
end