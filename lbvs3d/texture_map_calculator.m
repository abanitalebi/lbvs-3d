function texture_map = texture_map_calculator(frame_yuv_R, labels, fovea_radius, fovea_mask, grad)

    segsize = double(max(max(labels)));
    [height, width] = size(labels);
    
    % Gabor Energy Map
    right_Y = frame_yuv_R(:,:,1);
    Gmap_R = gabor_energy2D(right_Y);
    
    % Effect of fovea mask
    L = fovea_radius;
    Gmap_R_fovea = zeros(height, width);
    Gmap_padded = padarray(Gmap_R, [floor(fovea_radius/2),floor(fovea_radius/2)], 'symmetric');
    Gmap_padded = padarray(Gmap_padded, [L+1,L+1], 'replicate', 'post');
    [mp,np] = size(Gmap_padded);
    for i = 1:mp-2*L
        for j = 1:np-2*L
           Gmap_block = Gmap_padded(i:i+2*L, j:j+2*L);       % A block around each element
           Gmap_R_fovea(i,j) = sum(sum(Gmap_block .* fovea_mask )) / ((2*L+1)^2);
        end
    end

    % Emphasize on the salient texture area & De-emphasize on the redundant edges
    edgeness = grad;
    edgeness_map = zeros(height, width);
    for k = 1:segsize
        segment_mask = double(labels==k);
        edgeness_map = (sum(sum(segment_mask .* edgeness)) / sum(sum(segment_mask))) * segment_mask + edgeness_map;
    end
    texture_map1 = edgeness_map.*Gmap_R_fovea;
    texture_map1 = texture_map1 / max(max(texture_map1));
    
    % Emphasize on local maxima
    texture_map_maxima = emphasize_on_local_maxima(texture_map1);

    % Compactness & size effect: http://en.wikipedia.org/wiki/Image_moments
    compactness_map = compactness_map_generator(texture_map_maxima, segsize, labels);
   
    texture_map = texture_map_maxima .* compactness_map;
	% texture_map = double(imresize(texture_map, [height, width]));
    texture_map = texture_map / max(max(texture_map));
    
end