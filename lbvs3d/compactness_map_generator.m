function compactness_map = compactness_map_generator(conspicuty_map_maxima, regsize, labels)

    % Compactness effect: http://en.wikipedia.org/wiki/Image_moments
    MI = zeros(1,regsize);
    [height, width] = size(labels);
    compactness_map = zeros([height, width]);
    for k = 1:regsize
        segment_mask = double(labels==k);
        region = segment_mask .* conspicuty_map_maxima;
        x = repmat([1:height]', [1,width]);
        y = repmat(1:width, [height,1]);
        
        m00 = sum(sum(region));
        m01 = sum(sum(y.*region));
        m10 = sum(sum(x.*region));
        m02 = sum(sum((y.^2).*region));
        m20 = sum(sum((x.^2).*region));
       
        % Centroid
        xbar = m10/m00;
        ybar = m01/m00;
        
        mu00 = m00;
        mu02 = m02 - ybar*m01;
        mu20 = m20-xbar*m10;
        
        et02 = mu02 / (mu00^2);
        et20 = mu20 / (mu00^2);
        MI(k) = et20+et02;
        if isnan(MI(k)) 
            MI(k) = 0;
        end
        compactness_map = compactness_map + (MI(k) * segment_mask);
    end
    ma = max(max(compactness_map));
    size_mask = compactness_map<1*ma;
    compactness_map = size_mask.*compactness_map;
    compactness_map = 1-compactness_map/max(max(compactness_map));
    
end