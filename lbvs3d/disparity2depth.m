function depth_map = disparity2depth(disparity_map)

% computational model of stereoscopic 3d visual saliency: TIP 2013
% Units: cm
    V = 300;                % distance from the observer to the display
    I = 6.3;                % interoccular distance
    P = disparity_map;      % disparity
    W = 101.84;            % width of the display
    Rx = 1920;              % horizontal resolution of the display
    
   D = V ./ (1+ (I*W)./(P*Rx));         % wrong
%     D = V ./ (1+ (P*W)./(I*Rx));      % right
    
    depth_map = D;
    
end
