function depth_abruptness = depth_abruptness_generation (labels, depth_segmented, segsize, height, width, resize_factor)


    % Finding the difference map
%     diff_map = zeros(height, width);        xbar = zeros(1, segsize);       ybar = zeros(1, segsize);       depth = zeros(1, segsize);       
%     x = repmat([1:height]', [1,width]);
%     y = repmat(1:width, [height,1]);    
%     for i = 1:segsize
%         segment_mask = double(labels==i);
%         region = segment_mask .* depth_segmented;
%         m00 = sum(sum(region));
%         m01 = sum(sum(y.*region));
%         m10 = sum(sum(x.*region));
%         % Centroid
%         xbar(i) = m10/m00;
%         ybar(i) = m01/m00;
%         depth(i) = m00/sum(sum(segment_mask));
%     end
%     Centroid = [xbar ; ybar];
%     dists2 = pdist2(Centroid', Centroid', 'cityblock');   % Each row is the differences between the centroid of a segment and all other segments.
%     dists2(dist2==0) = 1;
%     weights2 = 1 ./ dists2;
%     depth_dist2 = pdist2(depth', depth', 'cityblock');
%     weighted_dist = depth_dist2 .* weights2;
%     diff_values = mean(weighted_dist, 2);
% %     diff_values = max(weighted_dist');
% %     diff_values = max(depth_dist2');
%     for k = 1:segsize
%         segment_mask = double(labels==k);
%         diff_map = segment_mask .* diff_values(k) + diff_map;
%     end
    step = floor(36 / resize_factor);       % default: 36
    x = repmat([1:height]', [1,width]);
    y = repmat(1:width, [height,1]);    
    diff_map = zeros(height, width);
    diffs = zeros(1, segsize);
    for k = 1:segsize
        segment_mask = double(labels==k);
        region = segment_mask .* depth_segmented;
        m00 = sum(sum(region));
        m01 = sum(sum(y.*region));
        m10 = sum(sum(x.*region));
        % Centroid
        xbar = m10/m00;
        ybar = m01/m00;
        [xx,yy] = find(segment_mask);
        point1 = round([max(xx)+ step, ybar]);
        point2 = round([min(xx)- step, ybar]);
        point3 = round([xbar, min(yy)- step]);
        point4 = round([xbar, max(yy)+ step]);
        if point1(1)>height
            point1(1)=height;
        end
        if point2(1)<1
            point2(1)=1;
        end
        if point3(2)<1
            point3(2)=1;
        end
        if point4(2)>width
            point4(2)=width;
        end
%         differences = [ (depth_segmented(point1(1),point1(2))-depth_segmented(round(xbar),round(ybar))) , (depth_segmented(point2(1),point2(2))-depth_segmented(round(xbar),round(ybar))) , (depth_segmented(point3(1),point3(2))-depth_segmented(round(xbar),round(ybar))) , (depth_segmented(point4(1),point4(2))-depth_segmented(round(xbar),round(ybar)))];
        distances = sqrt([ sum(([point1(1),point1(2)]-[xbar,ybar]).^2) , sum(([point2(1),point2(2)]-[xbar,ybar]).^2) , sum(([point3(1),point3(2)]-[xbar,ybar]).^2) , sum(([point4(1),point4(2)]-[xbar,ybar]).^2)]);
        differences = [ (depth_segmented(point1(1),point1(2))-depth_segmented(round(xbar),round(ybar))) , (depth_segmented(point2(1),point2(2))-depth_segmented(round(xbar),round(ybar))) , (depth_segmented(point3(1),point3(2))-depth_segmented(round(xbar),round(ybar))) , (depth_segmented(point4(1),point4(2))-depth_segmented(round(xbar),round(ybar)))];
        diff = mean(abs(differences./distances)) ;
        
        diff_map = segment_mask .* diff + diff_map;
        diffs(k) = diff;
    end
    
    diff_map_th = (diff_map>mean(diff_map(:))).*ones(height, width)*max(diff_map(:))+(diff_map<=mean(diff_map(:))).*diff_map;
    depth_segmented_norm = (depth_segmented - min(depth_segmented(:))) / (max(depth_segmented(:))-(min(depth_segmented(:))));
    depth_abruptness = diff_map_th.*depth_segmented_norm;
    depth_abruptness = depth_abruptness / (max(depth_abruptness(:)));
    
end