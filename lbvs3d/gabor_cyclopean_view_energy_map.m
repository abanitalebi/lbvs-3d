function CI_energy_map = gabor_cyclopean_view_energy_map (left_Y, right_Y, disparity_map)
    
    left_Y = double(left_Y);
    right_Y = double(right_Y);
    
    [m,n] = size(left_Y);
    
    CI = zeros(m,n);
    Gmap_L = gabor_energy2D(left_Y);
    Gmap_R = gabor_energy2D(right_Y);
    
    for i = 1:m
        for j = 1:n
            d = disparity_map (i,j) ;
            if (j-d <= n) && (j-d>=1)
                wl = Gmap_L (i,j) / ( Gmap_L(i,j) + Gmap_R(i,j-d) );
                wr = Gmap_R(i,j-d) / ( Gmap_L(i,j) + Gmap_R(i,j-d) );
                CI(i,j) = wl*left_Y(i,j) + wr*right_Y(i,j-d);
            end
            if (j-d > n)
                wl = Gmap_L (i,j) / ( Gmap_L(i,j) + Gmap_R(i,n) );
                wr = Gmap_R(i,n) / ( Gmap_L(i,j) + Gmap_R(i,n) );
                CI(i,j) = wl*left_Y(i,j) + wr*right_Y(i,n);
            end
            if (j-d < 1)
                wl = Gmap_L (i,j) / ( Gmap_L(i,j) + Gmap_R(i,1) );
                wr = Gmap_R(i,1) / ( Gmap_L(i,j) + Gmap_R(i,1) );
                CI(i,j) = wl*left_Y(i,j) + wr*right_Y(i,1);
            end
        end
    end
    CI_energy_map = CI;

end