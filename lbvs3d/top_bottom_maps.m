function [person_map, vehicle_map, animal_map, face_map, text_map, horizon_map] = top_bottom_maps(frame_R)
    
    [height, width, dim] = size(frame_R);
    im = frame_R;
    
    % Person detection
    person_map = zeros(height, width);
    load voc-release-3.1-win-master/VOC2008/person_final.mat
    bbox = process(im, model, 0);
    [m,n] = size(bbox);
    for i = 1:m
        x1 = round(bbox(i,1));
        y1 = round(bbox(i,2));
        x2 = round(bbox(i,3));
        y2 = round(bbox(i,4));
        person_map (y1:y2, x1:x2) = 1;
    end
    
    % vehicle detection: aeroplane, bicycle, boat, bus, car, motorbike, train
    vehicle_map = zeros(height, width);
    load voc-release-3.1-win-master/VOC2008/aeroplane_final.mat;                  bbox = process(im, model, 0);                 box = bbox;
    load voc-release-3.1-win-master/VOC2008/bicycle_final.mat;                    bbox = process(im, model, 0);                 box = [box; bbox];
    load voc-release-3.1-win-master/VOC2008/boat_final.mat;                       bbox = process(im, model, 0);                 box = [box; bbox];
    load voc-release-3.1-win-master/VOC2008/bus_final.mat;                        bbox = process(im, model, 0);                 box = [box; bbox];
    load voc-release-3.1-win-master/VOC2008/car_final.mat;                        bbox = process(im, model, 0);                 box = [box; bbox];  
    load voc-release-3.1-win-master/VOC2008/motorbike_final.mat;                  bbox = process(im, model, 0);                 box = [box; bbox];
    load voc-release-3.1-win-master/VOC2008/train_final.mat;                      bbox = process(im, model, 0);                 box = [box; bbox];
    load voc-release-3.1-win-master/VOC2008/boat_final.mat;                       bbox = process(im, model, 0);                 box = [box; bbox];
    [m,n] = size(box);
    for i = 1:m
        x1 = round(box(i,1));
        y1 = round(box(i,2));
        x2 = round(box(i,3));
        y2 = round(box(i,4));
        vehicle_map (y1:y2, x1:x2) = 1;
    end
    
    % animal detection: bird, cat, cow, dog, horse, sheep
    animal_map = zeros(height, width);
    load voc-release-3.1-win-master/VOC2008/bird_final.mat;                       bbox = process(im, model, 0);                   box = bbox;
    load voc-release-3.1-win-master/VOC2008/cat_final.mat;                        bbox = process(im, model, 0);                   box = [box; bbox];
    load voc-release-3.1-win-master/VOC2008/cow_final.mat;                        bbox = process(im, model, 0);                   box = [box; bbox];
    load voc-release-3.1-win-master/VOC2008/dog_final.mat;                        bbox = process(im, model, 0);                   box = [box; bbox];
    load voc-release-3.1-win-master/VOC2008/horse_final.mat;                      bbox = process(im, model, 0);                   box = [box; bbox];  
    load voc-release-3.1-win-master/VOC2008/sheep_final.mat;                      bbox = process(im, model, 0);                   box = [box; bbox];
    [m,n] = size(box);
    for i = 1:m
        x1 = round(box(i,1));
        y1 = round(box(i,2));
        x2 = round(box(i,3));
        y2 = round(box(i,4));
        animal_map (y1:y2, x1:x2) = 1;
    end
    
    % face detector
    faceDetector = vision.CascadeObjectDetector;
    box = step(faceDetector, im);
    face_map = zeros(height, width);
    [m,n] = size(box);
    for i = 1:m
        x1 = round(box(i,1));
        y1 = round(box(i,2));
        x2 = x1 + round(box(i,3));
        y2 = y1 + round(box(i,4));
        face_map (y1:y2, x1:x2) = 1;
    end
    
    
    % text detector
    I = im;
    text_map = zeros(height, width);
    results = ocr(I);
    box = results.WordBoundingBoxes;
    [m,n] = size(box);
     for i = 1:m
        x1 = round(box(i,1));
        y1 = round(box(i,2));
        x2 = x1 + round(box(i,3));
        y2 = y1 + round(box(i,4));
        if results.WordConfidences(i) > 0.1
            text_map (y1:y2, x1:x2) = 1;
        end
    end   
	text_map = double(imresize(text_map, [height, width]));
    
    
    % horizon detector
    h = getHorizon(im);
    horizon_map = zeros(height, width);
    horizon_map (round((h+0.5)*height)-10 : round((h+0.5)*height)+10, :) = 1;
    
end