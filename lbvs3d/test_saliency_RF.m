function saliency_map = test_saliency_RF(RFmodel, conspicuty_maps, type)
    
    features = conspicuty_maps;
    if strcmp(type, 'classification')
        predChar1 = RFmodel.predict(features);        % classification
        predictedClass = str2double(predChar1);    
    end
    if strcmp(type, 'regression')
        predictedClass = RFmodel.predict(features);          % regression
    end
        
    saliency_map = double(predictedClass) / double(max(predictedClass(:)));
    
end