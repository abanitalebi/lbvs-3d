
% Output = number of frames in a YUV video file
% Input = path, format, width, and height of the video file

function framenumber = yuv_frame_number(File,width,height,format)

    fwidth = 0.5;
    fheight= 0.5;
    if strcmp(format,'400')
        fwidth = 0;
        fheight= 0;
    elseif strcmp(format,'411')
        fwidth = 0.25;
        fheight= 1;
    elseif strcmp(format,'420')
        fwidth = 0.5;
        fheight= 0.5;
    elseif strcmp(format,'422')
        fwidth = 0.5;
        fheight= 1;
    elseif strcmp(format,'444')
        fwidth = 1;
        fheight= 1;
    else
        display('Error: wrong format');
    end
    %get Filesize and Framenumber
    filep = dir(File); 
    fileBytes = filep.bytes; %Filesize
    clear filep
    framenumber = fileBytes/(width*height*(1+2*fheight*fwidth)); %Framenumber
end

