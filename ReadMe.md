# LBVS-3D: 

## Learning-Based Visual Saliency model for Stereoscopic 3D Video

Disclaimer: This code is free for use only for research purposes. For any commrecial use please contact us at: amin [dot] banitalebi [at] gmail [dot] com

Please cite the following reference paper if you need to cite this code, or our DML-iTrack database:
  * [1] A. Banitalebi-Dehkordi, M. T. Pourazad, and Panos Nasiopoulos, “A Learning-Based Visual Saliency prediction model for stereoscopic 3D video (LBVS-3D),” 
				Springer Journal of Multimedia Tools and Applications, 2016, DOI 10.1007/s11042-016-4155-y.

    arxiv: https://arxiv.org/abs/1803.04842
    
    camera ready: http://ece.ubc.ca/~dehkordi/Files/papers/2016_MTAP_Dehkordi_LBVS-3D.pdf
    
    published: http://link.springer.com/article/10.1007/s11042-016-4155-y
    
    

## How to use the code (version 1.0):

-- warning: This code was written for prototyping algorithims, and proving concepts, and was not meant to be a production system code.

1) Compile/install third party packages: There are some third party packages required for LBVS-3D code to run, that are all available with this package. If needed, you would have to recompile or reinstall any of those packages for your platform/OS. This code is tested on Win7 x64 and MATLAB 2014.

2) Generate the 3D saliency features:
- This version of the software only supports stereo 3D video sequences in the format of "*.YUV" files. Copy your "*.YUV" files in an input directory. Note that current implementation is assuming addresses/filenaming that are associated to our DML-iTrack-3D database. 
- You will need three files: a left view YUV, a right view YUV, and a depth map video YUV. They are assumed to of the same number of frames and resolution. Modify the paths in code if necessary.
- Run ""generate_maps.m" script with proper parameters and paths.
- This code will generate various saliency features.

3) Generate saliency maps:
- Run the "map_fusion_RF_TestingOnly.m" script with the appropriate input features path. Note that since you are only using the framework in a testing mode (i.e. using the already generated model - the 8GB file), you won't need to set the training parameters. However, we provide the training code ("map_fusion_RF.m") as well, so you can create your own models tailored for your own datasets.

4) You can use "saliency_metrics_video_folders.m" to run various saliency metrics on the generated saliency maps. You need to have the fixation maps from eye-tracking experiments available for this.

Please feel free to send your questions to: amin [dot] banitalebi [at] gmail [dot] com












